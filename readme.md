# Spletna trgovina

EP 2015 / 2016


# Installation instructions

Clone this repository into `var/www/` using `git clone`.

Install dependencies with `composer install`. If you don't have composer, you can get it [here](https://getcomposer.org/)

Configure a new Apache virtual host with root folder int `var/www/EP-spletna-trgovina/public`, and with appropriate server name. Make sure to enable it afterwards using `a2enable` (or something like that).

Add server name to `/etc/hosts` file, so virtual host is accessible.

You should now be able to access the webpage using a web browser witht the server name you put into hosts and the virtual host config file.

*TODO* write provision script that automates the installation process.