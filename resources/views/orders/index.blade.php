@extends('layouts.master')

@section('title', 'Orders')

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Orders</h1>
        </div>
        <div class="panel-body">
            <p>Here you can edit your settings and personal information.</p>
        </div>
        <ul class="list-group">

            <li class="list-group-item">
                <h3>Pending orders ({{ $pendingOrders->count() }})</h3>
                @foreach($pendingOrders as $order)
                    <h4>{{ $order->item->display }}</h4>
                    <p>Quantity: <b>{{ $order->quantity }}</b></p>
                    <p>Placed on {{ $order->created_at }}</p>
                    @if(Auth::user()->isVendor())
                        {!! link_to_route('web.orders.show', 'See details', [$order]) !!}
                    @endif
                    <hr>
                @endforeach
            </li>

            @if(!$cancelledOrders->isEmpty())
                <li class="list-group-item">
                    <h3>Cancelled orders ({{ $cancelledOrders->count() }})</h3>
                    @foreach($cancelledOrders as $order)
                        <h4>{{ $order->item->display }}</h4>
                        <p>Quantity: <b>{{ $order->quantity }}</b></p>
                        <p>Placed on {{ $order->created_at }}</p>
                        @if(Auth::user()->isVendor())
                            {!! link_to_route('web.orders.show', 'See details', [$order]) !!}
                        @endif
                        <hr>
                    @endforeach
                </li>
            @endif

            @if(!$pastOrders->isEmpty())
                <li class="list-group-item">
                    <h3>Past orders ({{ $pastOrders->count() }})</h3>
                    @foreach($pastOrders as $order)
                        <h4>{{ $order->item->display }}</h4>
                        <p>Quantity: <b>{{ $order->quantity }}</b></p>
                        <p>Placed on {{ $order->created_at }}</p>
                        @if(Auth::user()->isVendor())
                            {!! link_to_route('web.orders.show', 'See details', [$order]) !!}
                        @endif
                        <hr>
                    @endforeach
                </li>
            @endif

        </ul>
    </div>
@stop