@extends('layouts.master')

@section('title', 'Order details')

@section('main-content')

    <div class="panel panel-default">
        <div class="panel-heading"><h1>Viewing details for order {{ $order->id }}</h1></div>
        <div class="panel-body">
            <dl class="dl-horizontal">
                <dt>Id</dt>
                <dd>{{ $order->id }}</dd>
                <dt>Order placed</dt>
                <dd>{{ $order->created_at }}</dd>
                <dt>Item</dt>
                <dd>{!! link_to_route('web.items.retrieve', $order->item->display, [$order->item]) !!}</dd>
                <dt>Status</dt>
                <dd>{{ App\Models\Order::$orderStatus[$order->status] }}</dd>
            </dl>

            {!! Form::open(['route' => ['web.orders.updateStatus', $order], 'method' => 'put']) !!}
            <b>Change status</b>
            {!! Form::select('status', App\Models\Order::$orderStatus, $order->status, ['class' => 'form-control']) !!}
            {!! Form::submit('Update', ['class' => 'btn btn-default']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop