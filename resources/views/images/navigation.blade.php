<li class="list-group-item">
    <ol class="breadcrumb">
        <li>{!! link_to_route('web.selling.index', 'Selling') !!}</li>
        <li>{!! link_to_route('web.items.edit', $item->display, $item->id) !!}</li>
        <li>{!! link_to_route('web.items.images.index', 'Images', $item->id) !!}</li>
    </ol>
    <ul class="nav nav-pills">
        <li role="presentation" {{ $selected == 'all' ? 'class=active' : '' }}>{!! link_to_route('web.items.images.index', 'All images', $item->id) !!}</li>
        <li role="presentation" {{ $selected == 'new' ? 'class=active' : '' }}>{!! link_to_route('web.items.images.create', 'Upload new image', $item->id) !!}</li>
        <li role="presentation" {{ $selected == 'profile' ? 'class=active' : '' }}>{!! link_to_route('web.items.images.profile', 'Select a profile picture', $item->id) !!}</li>
    </ul>
</li>