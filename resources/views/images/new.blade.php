@extends('layouts.master')

@section('title', 'Editing item ' . $item->display . ' images')

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Editing item {{ $item->display }} &raquo;
                <small>Images</small>
            </h1>
        </div>

        <ul class="list-group">
            @include('images.navigation', ['selected' => 'new'])
            <li class="list-group-item">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Upload a new image</h3>
                    </div>
                    <div class="col-md-12">
                        {!! Form::open(['route' => ['web.items.images.store', $item->id], 'files' => true]) !!}
                        {!! Form::file('images[]') !!}
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-12">
                        {!! Form::submit('Upload', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </li>
        </ul>
    </div>
@stop