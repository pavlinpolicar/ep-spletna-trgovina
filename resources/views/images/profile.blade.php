@extends('layouts.master')

@section('title', 'Editing item ' . $item->display . ' images')

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Editing item {{ $item->display }} &raquo;
                <small>Images</small>
            </h1>
        </div>

        <ul class="list-group">
            @include('images.navigation', ['selected' => 'profile'])
            <li class="list-group-item">
                <div class="row">
                    <div class="col-md-12">
                        <h3>All images</h3>
                    </div>
                    @if(!$images->isEmpty())
                        @foreach($images as $image)
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body row">
                                        <div class="col-md-3">
                                            <img src="{{ $image->url }}" alt="" width="100%">
                                        </div>
                                        <div class="col-md-9">
                                            <p>({{ $image->name }})</p>
                                            @if($item->displayImage !== null and $item->displayImage->id === $image->id)
                                                <p><b>DISPLAY</b></p>
                                            @endif
                                            <p>
                                                {!! Form::open(['route' => ['web.items.images.profile.select', $item->id, $image->name], 'method' => 'put']) !!}
                                                {!! Form::submit('Select', ['class' => 'btn btn-primary']) !!}
                                                {!! Form::close() !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-md-12">
                            <p><i>You must upload an image before you can selet a dispaly image.</i>
                            </p>
                        </div>

                    @endif
                </div>
            </li>
        </ul>
    </div>
@stop