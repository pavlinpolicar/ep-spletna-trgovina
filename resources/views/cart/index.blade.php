@extends('layouts.master')

@section('title', 'Items')

@section('main-content')
    <div class="row">
        <h1>These are the items</h1>

        <h3>Total: {{ $items->total() }}</h3>

    </div>
    <div class="row">
        @foreach($items as $item)
            <div class="panel panel-default">
                <div class="panel-body row">
                    <div class="col-md-2">
                        <a href="{!! route('web.items.retrieve', $item->id) !!}" class="thumbnail">
                            @if($item->displayImage)
                                <img src="{!! $item->displayImage->url !!}" alt="">
                            @else
                                <img src="https://placehold.it/150x150" alt="">
                            @endif
                        </a>
                    </div>
                    <div class="col-md-10">
                        <h3>{!! link_to_route('web.items.retrieve', $item->display, [$item->id]) !!}</h3>
                        <p>{!! $item->description !!}</p>

                        {!! Form::open(['route' => ['web.cart.updateItem', $item->id], 'method' => 'put', 'class' => 'form-inline']) !!}
                        <div class="input-group">
                            {!! Form::text('quantity', $item->pivot->quantity, ['class' => 'form-control', 'size' => 3]) !!}
                            <span class="input-group-btn">
                            {!! Form::submit('Update', ['class' => 'btn btn-default']) !!}
                            </span>
                        </div>
                        {!! Form::close() !!}

                        {!! Form::open(['route' => ['web.cart.removeItem', $item->id], 'method' => 'delete', 'class' => 'form-inline']) !!}
                        {!! Form::submit('Remove item', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}

                        <p>Added {{ $item->pivot->created_at->diffForHumans() }}</p>
                    </div>
                </div>
            </div>
        @endforeach
        @if($items->total() > $items->perPage())
            <nav>
                <ul class="pagination">
                    @if($items->previousPageUrl() or $items->currentPage() !== 1)
                        <li><a href="{!! $items->previousPageUrl() !!}"><span>&laquo;</span></a>
                        </li>
                    @endif
                    {{--*/ $numPages = ceil($items->total() / $items->perPage()) /*--}}
                    @for($i = 1; $i <= $numPages; ++$i)
                        <li{{ $i === $items->currentPage() ? ' class=active' : '' }}>
                            <a href="{!! $items->url($i) !!}">{{ $i }}</a>
                        </li>
                    @endfor
                    @if($items->nextPageUrl())
                        <li><a href="{!! $items->nextPageUrl() !!}"><span>&raquo;</span></a></li>
                    @endif
                </ul>
            </nav>
        @endif

    </div>

    <div class="row">
        <div class="col-md-12">
            {!! link_to_route('web.checkout.index', 'Proceed to checkout', [], ['class' => 'btn btn-default']) !!}
        </div>
    </div>
@stop