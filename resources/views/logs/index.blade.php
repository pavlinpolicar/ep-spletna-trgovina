@extends('layouts.master')

@section('title', 'Add a new customer')

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Logs</h1>
        </div>
        <div class="panel-body">
            <table class="table">
                @foreach($logs as $log)
                    <tr>
                        <td>{{ $log->id }}</td>
                        <td>{{ $log->user->name . ' ' . $log->user->surname . ' (' . $log->user->id . ')' }}</td>
                        <td>{{ $log->description }}</td>
                        <td>{{ $log->created_at }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    @if($logs->total() > $logs->perPage())
        <nav>
            <ul class="pagination">
                @if($logs->previousPageUrl() or $logs->currentPage() !== 1)
                    <li><a href="{!! $logs->previousPageUrl() !!}"><span>&laquo;</span></a></li>
                @endif
                {{--*/ $numPages = ceil($logs->total() / $logs->perPage()) /*--}}
                @for($i = 1; $i <= $numPages; ++$i)
                    <li{{ $i === $logs->currentPage() ? ' class=active' : '' }}>
                        <a href="{!! $logs->url($i) !!}">{{ $i }}</a>
                    </li>
                @endfor
                @if($logs->nextPageUrl())
                    <li><a href="{!! $logs->nextPageUrl() !!}"><span>&raquo;</span></a></li>
                @endif
            </ul>
        </nav>
    @endif
@stop