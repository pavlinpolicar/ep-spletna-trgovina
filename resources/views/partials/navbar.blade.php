<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {!! link_to_route('web.items.index', 'EP Store', [], ['class' => 'navbar-brand']) !!}
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                @if(Auth::check() and Auth::user()->isVendor())
                    <li>{!! link_to_route('web.selling.index', 'Selling') !!}</li>
                @endif
                <li>{!! link_to_route('web.items.index', 'Items') !!}</li>
                @if(Auth::check())
                    @if(Auth::user()->isVendor())
                        <li>
                            <a href="{!! route('web.orders.index') !!}">
                                Orders
                                <span class="badge">{{ Auth::user()->getNewOrders()->count() }}</span>
                            </a>
                        </li>
                    @elseif(Auth::user()->isAdmin())
                        <li>{!! link_to_route('web.logs.index', 'Logs') !!}</li>
                    @else
                        <li>{!! link_to_route('web.orders.index', 'Orders') !!}</li>
                    @endif
                @endif
            </ul>
            <ul class="nav  navbar-nav navbar-right">
                @if(Auth::check())
                    <li class="navbar-text">Hello {{ Auth::user()->name }}</li>
                    @if(Auth::user()->isCustomer())
                        <li><a href="{!! route('web.cart.index') !!}">
                                <span class="glyphicon glyphicon glyphicon-shopping-cart"></span>
                                Cart
                                <span class="badge">{{ Auth::user()->cart->count() }}</span></a>
                        </li>
                    @endif
                    <li>{!! link_to_route('web.settings.index', 'Settings') !!}</li>
                    <li>{!! link_to_route('web.logout', 'Log out') !!}</li>
                @else
                    <li>{!! link_to_route('web.login', 'Log in') !!}</li>
                @endif
            </ul>
        </div>
    </div>
</nav>