<div class="form-group{{ $errors->has($field) ? ' has-error' : ''}}">
    {!! Form::label($field, isset($display) ? $display : ucfirst($field), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9">
        @if(isset($type) and $type === 'password')
            {!! Form::password($field, ['class' => 'form-control']) !!}
        @elseif(isset($type) and $type === 'textarea')
            {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => isset($rows) ? $rows : 3]) !!}
        @else
            {!! Form::text($field, null, ['class' => 'form-control']) !!}
        @endif
    </div>
    @if($errors->has($field))
        <div class="col-md-offset-3 col-md-9">
            <br>
            <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign"></span>
                @foreach($errors->get($field) as $error)
                    {{ $error }}<br>
                @endforeach
            </div>
        </div>
    @endif
</div>