@extends('layouts.master')

@section('title', 'Add a new customer')

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            @if(Auth::user()->isVendor())
                <h1>Add a new customer</h1>
            @elseif(Auth::user()->isAdmin())
                <h1>Add a new vendor</h1>
            @endif
        </div>
        <div class="panel-body">
            <ol class="breadcrumb">
                <li>{!! link_to_route('web.settings.index', 'Settings') !!}</li>
                <li>{!! link_to_route('web.users.new', 'New user') !!}</li>
            </ol>
            {!! Form::open(['route' => 'web.users.create', 'class' => 'form-horizontal']) !!}
            @include('partials.horizontal-form-element', ['field' => 'name'])
            @include('partials.horizontal-form-element', ['field' => 'surname'])
            @include('partials.horizontal-form-element', ['field' => 'email'])
            @include('partials.horizontal-form-element', ['field' => 'password', 'type' => 'password'])
            @include('partials.horizontal-form-element', ['field' => 'confirm_password', 'display' => 'Confirm password', 'type' => 'password'])
            @if(Auth::user()->isVendor())
                @include('partials.horizontal-form-element', ['field' => 'address', 'display' => 'Home address'])
                @include('partials.horizontal-form-element', ['field' => 'telephone', 'display' => 'Telephone number'])
            @endif
            @if(Auth::user()->isVendor())
                {!! Form::hidden('permission_level', App\Models\PermissionLevel::CUSTOMER) !!}
            @elseif(Auth::user()->isAdmin())
                {!! Form::hidden('permission_level', App\Models\PermissionLevel::VENDOR) !!}
            @endif
            <div class="form-group">
                <div class="col-md-offset-3 col-md-9">
                    {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop