@extends('layouts.master')

@section('title', 'Add a new customer')

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Editing {{ $user->name }}</h1>
        </div>
        <ul class="list-group">

            <li class="list-group-item">
                <ol class="breadcrumb">
                    <li>{!! link_to_route('web.settings.index', 'Settings') !!}</li>
                    <li>{!! link_to_route('web.users.index', 'Users') !!}</li>
                    <li>{!! link_to_route('web.users.edit', $user->name . ' ' . $user->surname) !!}</li>
                </ol>
                <div class="col-md-offset-3 col-md-9">
                    <h3>Personal infomation</h3>
                </div>
                {!! Form::model($user, ['route' => ['web.users.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('surname', 'Surname', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::text('surname', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                @if($user->isCustomer())
                    <div class="form-group">
                        {!! Form::label('address', 'Address', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('address', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('telephone', 'Telephone number', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-9">
                            {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </li>

            <li class="list-group-item">
                <div class="col-md-offset-3 col-md-9">
                    <h3>Change password</h3>
                </div>
                {!! Form::open(['route' => ['web.users.update', $user], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('password', 'Password', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Confirm password', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-9">
                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </li>

            <li class="list-group-item">
                <div class="col-md-offset-3 col-md-9">
                    <h3>Active</h3>
                </div>
                {!! Form::model($user, ['route' => ['web.users.update', $user], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('active', 1, null) !!} Active
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </li>
        </ul>
    </div>
@stop