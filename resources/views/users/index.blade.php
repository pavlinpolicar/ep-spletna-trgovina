@extends('layouts.master')

@section('title', 'Add a new customer')

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>All customers</h1>
        </div>
        <div class="panel-body">
            <ol class="breadcrumb">
                <li>{!! link_to_route('web.settings.index', 'Settings') !!}</li>
                <li>{!! link_to_route('web.users.index', 'Users') !!}</li>
            </ol>
            <table class="table">
                @foreach($users as $user)
                    @if($user->isActive())
                        <tr>
                    @else
                        <tr class="info">
                            @endif
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->surname }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>{!! link_to_route('web.users.edit', 'Edit', $user->id) !!}</td>
                        </tr>
                        @endforeach
            </table>
        </div>
    </div>

    @if($users->total() > $users->perPage())
        <nav>
            <ul class="pagination">
                @if($users->previousPageUrl() or $users->currentPage() !== 1)
                    <li><a href="{!! $users->previousPageUrl() !!}"><span>&laquo;</span></a></li>
                @endif
                {{--*/ $numPages = ceil($users->total() / $users->perPage()) /*--}}
                @for($i = 1; $i <= $numPages; ++$i)
                    <li{{ $i === $users->currentPage() ? ' class=active' : '' }}>
                        <a href="{!! $users->url($i) !!}">{{ $i }}</a>
                    </li>
                @endfor
                @if($users->nextPageUrl())
                    <li><a href="{!! $users->nextPageUrl() !!}"><span>&raquo;</span></a></li>
                @endif
            </ul>
        </nav>
    @endif
@stop