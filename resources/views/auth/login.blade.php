@extends('layouts.master')

@section('title', 'Login')

@section('main-content')
    {!! Form::open(['route' => 'web.login.post', 'class' => 'form-signin']) !!}
    <h2 class="form-signin-heading">Please sign in</h2>
    {!! Form::label('email', 'Email address', ['class' => 'sr-only']) !!}
    {!! Form::email('email', '', ['class' => 'form-control', 'required', 'autofocus', 'placeholder' => 'example@example.com']) !!}

    {!! Form::label('password', 'Password', ['class' => 'sr-only']) !!}
    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required']) !!}
    <div class="checkbox">
        <label>
            {!! Form::checkbox('remember-me', 'Remember me') !!} Remember me
        </label>
    </div>
    {!! Form::submit('Sign in', ['class' => 'btn btn-lg btn-primary btn-block']) !!}
    {!! Form::close() !!}
    @if(count($errors) > 0)
        The credentials you entered were not correct.
    @endif
    <div class="col-md-12 text-center">
        <p>Don't have an account yet? {!! link_to_route('web.register', 'Register') !!}!
        <br>It only takes a minute.</p>
    </div>

@stop