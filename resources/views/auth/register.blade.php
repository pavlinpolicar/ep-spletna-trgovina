@extends('layouts.master')

@section('title', 'Registration')

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Register</h1>
        </div>
        <div class="panel-body">
            <p>Please fill out the required information.</p>
        </div>
        <ul class="list-group">

            <li class="list-group-item">
                {!! Form::open(['route' => 'web.register.post', 'class' => 'form-horizontal']) !!}
                @include('partials.horizontal-form-element', ['field' => 'name'])
                @include('partials.horizontal-form-element', ['field' => 'surname'])
                @include('partials.horizontal-form-element', ['field' => 'email'])
                @include('partials.horizontal-form-element', ['field' => 'address', 'display' => 'Home address'])
                @include('partials.horizontal-form-element', ['field' => 'telephone', 'display' => 'Telephone number'])
                @include('partials.horizontal-form-element', ['field' => 'password', 'type' => 'password'])
                @include('partials.horizontal-form-element', ['field' => 'confirm_password', 'display' => 'Confirm password', 'type' => 'password'])
                {!! Form::hidden('permission_level', App\Models\PermissionLevel::CUSTOMER) !!}
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </li>

        </ul>
    </div>
@stop