@extends('layouts.master')

@section('title', 'Items')

@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <h1>These are the items</h1>

            <h3>Number of items: {{ count($items) }}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul>
                @foreach($items as $item)
                    <div class="panel panel-default">
                        <div class="panel-body row">
                            <div class="col-md-2">
                                <a href="{!! route('web.items.retrieve', $item->id) !!}"
                                   class="thumbnail">
                                    @if($item->displayImage)
                                        <img src="{!! $item->displayImage->url !!}" alt="">
                                    @else
                                        <img src="https://placehold.it/150x150" alt="">
                                    @endif
                                </a>
                            </div>
                            <div class="col-md-10">
                                <h3>{!! link_to_route('web.items.retrieve', $item->display, [$item->id]) !!}</h3>
                                <p>{!! $item->description !!}</p>
                                <p>Quantity: <b>{{ $item->pivot->quantity }}</b></p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route' => 'web.checkout.post']) !!}
            {!! Form::submit('Complete purchase', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop