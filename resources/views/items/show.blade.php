@extends('layouts.master')

@section('title', $item->display)

@section('main-content')


    <div class="panel panel-default"{{ $item->active ? '' : ' style=opacity:0.75' }}>
        <ul class="list-group">

            <li class="list-group-item row">
                <div class="col-md-3">
                    <img src="{{ $item->display_image ?
                URL::asset($item->getImageDirectory() . $item->display_image->name) :
                'https://placehold.it/200x200' }}" alt="" width="100%" class="img-thumbnail">
                </div>
                <div class="col-md-9">
                    <h1>{{ $item->display }}</h1>
                    <p>{{ $item->description }}</p>
                    <em>{{ $item->price }}&nbsp;&euro;</em>
                    <br>
                    <b>Rating:
                        @if($item->numRatings > 0)
                            {{ number_format((float)$item->averageRating, 1, '.', '') }}
                            (rated {{ $item->numRatings }} times)
                        @else
                            <i>This item has not been rated yet.</i>
                        @endif
                    </b>

                    @if(Auth::check() and Auth::user()->owns($item))
                        {!! link_to_route('web.items.edit', 'Edit', $item->id, ['class' => 'btn btn-default']) !!}
                    @endif

                    @if(Auth::check())
                        {!! Form::open(['route' => ['web.items.ratings.create', $item->id], 'class' => 'form-inline']) !!}
                        <div class="input-group">
                            {!! Form::text('rating', '', ['class' => 'form-control', 'size' => 3]) !!}
                            <span class="input-group-btn">
                            {!! Form::submit('Rate', ['class' => 'btn btn-default']) !!}
                        </span>
                        </div>
                        {!! Form::close() !!}
                    @else
                        <p><i>You must be logged in to rate items.</i></p>
                    @endif

                    <br>

                    @if(Auth::check() and !Auth::user()->hasItemInCart($item))
                        {!! Form::open(['route' => ['web.cart.addItem', $item->id], 'class' => 'form-inline']) !!}
                        <div class="input-group">
                            {!! Form::text('quantity', 1, ['class' => 'form-control', 'size' => 3]) !!}
                            <span class="input-group-btn">
                            {!! Form::submit('Add to cart', ['class' => 'btn btn-default']) !!}
                            </span>
                        </div>
                        {!! Form::close() !!}
                    @elseif(Auth::check())
                        <p><i>You already have this item in your cart.</i></p>
                    @else
                        <p><i>You must be logged in to add items to your cart.</i></p>
                    @endif
                </div>
            </li>
            <li class="list-group-item row">
                <div class="col-md-12">
                    <h3>Image gallery</h3>
                </div>
                @foreach($item->images as $image)
                    <div class="col-md-3">
                        <img src="{{
                            URL::asset($item->getImageDirectory() . $image->name)
                        }}" alt="" width="100%" class="img-thumbnail">
                    </div>
                @endforeach
                @if($item->images->isEmpty())
                    <div class="col-md-12">
                        <i>No pictures have yet been added.</i>
                    </div>
                @endif
            </li>

            <li class="list-group-item row">
                <div class="col-md-12">
                    <h3>Reviews</h3>
                </div>
                @foreach($item->ratings as $rating)
                    @if($rating->review)
                        <div class="col-md-12">
                            <p><b>{{ $rating->user->name }}</b> rated this item
                                with {{ $rating->rating}}
                            </p>
                            <p>{{ $rating->review }}</p>
                        </div>
                    @endif
                @endforeach
                @if($item->ratings->filter(function ($r) { return $r->review !== '' and $r->review !== null; })->isEmpty())
                    <div class="col-md-12">
                        <p><i>This item doesn't have any reviews yet.</i></p>
                    </div>
                @endif
            </li>

        </ul>
    </div>
@stop