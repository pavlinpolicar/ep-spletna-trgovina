@extends('layouts.master')

@section('title', 'Items')

@section('main-content')
    <h1>These are the items</h1>

    <h3>Total: {{ $items->total() }}</h3>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    {!! Form::open(['route' => 'web.items.index', 'method' => 'get']) !!}
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                        </span>
                        <input type="text" class="form-control" placeholder="Search for..." name="query">
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <p><b>Sort by:</b></p>
                </div>
                <div class="col-md-12">
                    {!! link_to_route('web.items.index', 'Display asc', ['sort' => 'display.asc']) !!}
                    {!! link_to_route('web.items.index', 'Display desc', ['sort' => 'display.desc']) !!}
                    {!! link_to_route('web.items.index', 'Price asc', ['sort' => 'price.asc']) !!}
                    {!! link_to_route('web.items.index', 'Price desc', ['sort' => 'price.desc']) !!}
                </div>
            </div>
        </div>
    </div>

    @foreach($items as $item)
        <div class="panel panel-default"{{ $item->active ? '' : ' style=opacity:0.75' }}>
            <div class="panel-body row">
                <div class="col-md-2">
                    <a href="{!! route('web.items.retrieve', $item->id) !!}" class="thumbnail">
                        @if($item->displayImage)
                            <img src="{!! $item->displayImage->url !!}" alt="">
                        @else
                            <img src="https://placehold.it/150x150" alt="">
                        @endif
                    </a>
                </div>
                <div class="col-md-10">
                    <h3>{!! link_to_route('web.items.retrieve', $item->display, [$item->id]) !!}</h3>
                    <p>{{ $item->description }}</p>
                    @can('update', $item)
                    {!! link_to_route('web.items.edit', 'Edit', [$item]) !!}
                    @endcan
                </div>
            </div>
        </div>
    @endforeach

    @if($items->total() > $items->perPage())
        <nav>
            <ul class="pagination">
                @if($items->previousPageUrl() or $items->currentPage() !== 1)
                    <li><a href="{!! $items->previousPageUrl() !!}"><span>&laquo;</span></a></li>
                @endif
                {{--*/ $numPages = ceil($items->total() / $items->perPage()) /*--}}
                @for($i = 1; $i <= $numPages; ++$i)
                    <li{{ $i === $items->currentPage() ? ' class=active' : '' }}>
                        <a href="{!! $items->url($i) !!}">{{ $i }}</a>
                    </li>
                @endfor
                @if($items->nextPageUrl())
                    <li><a href="{!! $items->nextPageUrl() !!}"><span>&raquo;</span></a></li>
                @endif
            </ul>
        </nav>
    @endif
@stop