@extends('layouts.master')

@section('title', 'Editing item ' . $item->display)

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Editing item {{ $item->display }}</h1>
        </div>

        <ul class="list-group">
            <li class="list-group-item">
                <ol class="breadcrumb">
                    <li>{!! link_to_route('web.selling.index', 'Selling') !!}</li>
                    <li>{!! link_to_route('web.items.edit', $item->display, $item->id) !!}</li>
                </ol>
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <h3>Basic information</h3>
                    </div>
                    {!! Form::model($item, ['route' => ['web.items.update', $item->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                    @include('partials.horizontal-form-element', ['field' => 'display'])
                    @include('partials.horizontal-form-element', ['field' => 'description', 'type' => 'textarea'])
                    @include('partials.horizontal-form-element', ['field' => 'price'])

                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-9">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('active', 1, null) !!} Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </li>

            <li class="list-group-item">
                {!! link_to_route('web.items.images.index', 'Manage item images', [$item->id], ['class' => 'btn btn-default']) !!}
            </li>
        </ul>
    </div>
@stop