@extends('layouts.master')

@section('title', 'Items')

@section('main-content')
    <h1>Selling</h1>

    <h3>Total: {{ $items->total() }}</h3>

    {!! link_to_route('web.items.new', 'Add a new item') !!}

    @foreach($items as $item)
        <div class="panel panel-default"{{ $item->active ? '' : ' style=opacity:0.75' }}>
            <div class="panel-body row">
                <div class="col-md-2">
                    <a href="{!! route('web.items.retrieve', $item->id) !!}" class="thumbnail">
                        @if($item->displayImage)
                            <img src="{!! $item->displayImage->url !!}" alt="">
                        @else
                            <img src="https://placehold.it/150x150" alt="">
                        @endif
                    </a>
                </div>
                <div class="col-md-10">
                    <h3>{!! link_to_route('web.items.retrieve', $item->display, [$item->id]) !!}</h3>
                    <p>{{ $item->description }}</p>
                    @can('update', $item)
                    {!! link_to_route('web.items.edit', 'Edit', [$item]) !!}
                    @endcan
                </div>
            </div>
        </div>
    @endforeach

    @if($items->total() > $items->perPage())
        <nav>
            <ul class="pagination">
                @if($items->previousPageUrl() or $items->currentPage() !== 1)
                    <li><a href="{!! $items->previousPageUrl() !!}"><span>&laquo;</span></a></li>
                @endif
                {{--*/ $numPages = ceil($items->total() / $items->perPage()) /*--}}
                @for($i = 1; $i <= $numPages; ++$i)
                    <li{{ $i === $items->currentPage() ? ' class=active' : '' }}>
                        <a href="{!! $items->url($i) !!}">{{ $i }}</a>
                    </li>
                @endfor
                @if($items->nextPageUrl())
                    <li><a href="{!! $items->nextPageUrl() !!}"><span>&raquo;</span></a></li>
                @endif
            </ul>
        </nav>
    @endif
@stop