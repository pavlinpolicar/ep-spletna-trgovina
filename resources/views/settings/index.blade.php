@extends('layouts.master')

@section('title', 'Settings')

@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Settings</h1>
        </div>
        <div class="panel-body">
            <p>Here you can edit your settings and personal information.</p>
        </div>
        <ul class="list-group">

            <li class="list-group-item">
                <div class="col-md-offset-3 col-md-9">
                    <h3>Personal infomation</h3>
                </div>
                {!! Form::model($user, ['route' => 'web.settings.info.save', 'class' => 'form-horizontal']) !!}
                @include('partials.horizontal-form-element', ['field' => 'name'])
                @include('partials.horizontal-form-element', ['field' => 'surname'])
                @include('partials.horizontal-form-element', ['field' => 'email'])
                @if(Auth::user()->isCustomer())
                    @include('partials.horizontal-form-element', ['field' => 'address', 'display' => 'Home address'])
                    @include('partials.horizontal-form-element', ['field' => 'telephone', 'display' => 'Telephone number'])
                @endif
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </li>

            <li class="list-group-item">
                <div class="col-md-offset-3 col-md-9">
                    <h3>Change password</h3>
                </div>
                {!! Form::open(['route' => 'web.settings.password.save', 'class' => 'form-horizontal']) !!}
                @include('partials.horizontal-form-element', ['field' => 'password', 'type' => 'password'])
                @include('partials.horizontal-form-element', ['field' => 'password_confirmation', 'display' => 'Confirm password', 'type' => 'password'])
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </li>

            @if(Auth::user()->isVendor() or Auth::user()->isAdmin())
                <li class="list-group-item">
                    {!! link_to_route('web.users.new', 'Create a new user') !!}
                    {!! link_to_route('web.users.index', 'See users') !!}
                </li>
            @endif

        </ul>
    </div>
@stop