<?php

namespace tests\Policies;


use App\Models\Item;
use App\Models\User;
use App\Policies\ItemPolicy;
use EG;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use TestCase;

class ItemPolicyTest extends TestCase
{
    use DatabaseTransactions;

    public function createFields()
    {
        $this->policy = app(ItemPolicy::class);

        $this->admin = EG::admin();
        $this->admin2 = EG::admin();
        $this->vendor = EG::vendor();
        $this->vendor2 = EG::vendor();
        $this->customer = EG::customer();
        $this->customer2 = EG::customer();

        $this->itemVendor = $this->giveItemToUser($this->vendor);
        $this->itemVendor2 = $this->giveItemToUser($this->vendor2);
    }

    /**
     * @covers App\Policies\ItemPolicy::create
     * @group acl
     */
    public function testCreateItem()
    {
        $this->createFields();

        $this->assertEquals(false, $this->policy->create($this->admin));
        $this->assertEquals(true, $this->policy->create($this->vendor));
        $this->assertEquals(false, $this->policy->create($this->customer));
    }

    /**
     * Check that anyone can give ratings to any items.
     *
     * @covers App\Policies\ItemPolicy::createRating
     * @group acl
     */
    public function testCreateItemRating()
    {
        $this->createFields();

        $this->assertEquals(true, $this->policy->createRating($this->admin, $this->itemVendor));
        $this->assertEquals(true, $this->policy->createRating($this->vendor, $this->itemVendor));
        $this->assertEquals(true, $this->policy->createRating($this->customer, $this->itemVendor));

        $this->assertEquals(true, $this->policy->createRating($this->admin, $this->itemVendor2));
        $this->assertEquals(true, $this->policy->createRating($this->vendor, $this->itemVendor2));
        $this->assertEquals(true, $this->policy->createRating($this->customer, $this->itemVendor2));
    }

    /**
     * @covers App\Policies\ItemPolicy::update
     * @group acl
     */
    public function testUpdateItem()
    {
        $this->createFields();

        $this->assertEquals(false, $this->policy->update($this->admin, $this->itemVendor));
        $this->assertEquals(true, $this->policy->update($this->vendor, $this->itemVendor));
        $this->assertEquals(false, $this->policy->update($this->customer, $this->itemVendor));

        $this->assertEquals(false, $this->policy->update($this->vendor, $this->itemVendor2));
    }

    /**
     * @covers App\Policies\ItemPolicy::delete
     * @group acl
     */
    public function testDeleteItem()
    {
        $this->createFields();

        $this->assertEquals(false, $this->policy->delete($this->admin, $this->itemVendor));
        $this->assertEquals(true, $this->policy->delete($this->vendor, $this->itemVendor));
        $this->assertEquals(false, $this->policy->delete($this->customer, $this->itemVendor));

        $this->assertEquals(false, $this->policy->delete($this->vendor, $this->itemVendor2));
    }

    protected function giveItemToUser(User $user)
    {
        return Mockery::mock(Item::class)
            ->makePartial()
            ->shouldReceive('getVendorAttribute')
            ->andReturn($user)
            ->mock();
    }
}