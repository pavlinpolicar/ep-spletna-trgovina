<?php

namespace tests\Policies;


use App\Models\Rating;
use App\Models\User;
use App\Policies\RatingPolicy;
use EG;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use TestCase;

class RatingPolicyTest extends TestCase
{
    use DatabaseTransactions;

    public function createFields()
    {
        $this->policy = app(RatingPolicy::class);

        $this->admin = EG::admin();
        $this->admin2 = EG::admin();
        $this->vendor = EG::vendor();
        $this->vendor2 = EG::vendor();
        $this->customer = EG::customer();
        $this->customer2 = EG::customer();

        $this->ratingAdmin = $this->giveRatingToUser($this->admin);
        $this->ratingVendor = $this->giveRatingToUser($this->vendor);
        $this->ratingCustomer = $this->giveRatingToUser($this->customer);
        $this->ratingAdmin2 = $this->giveRatingToUser($this->admin2);
        $this->ratingVendor2 = $this->giveRatingToUser($this->vendor2);
        $this->ratingCustomer2 = $this->giveRatingToUser($this->customer2);
    }

    /**
     * @covers App\Policies\RatingPolicy::update
     * @group acl
     */
    public function testUpdateRating()
    {
        $this->createFields();

        $this->assertEquals(true, $this->policy->update($this->admin, $this->ratingAdmin));
        $this->assertEquals(true, $this->policy->update($this->vendor, $this->ratingVendor));
        $this->assertEquals(true, $this->policy->update($this->customer, $this->ratingCustomer));

        $this->assertEquals(false, $this->policy->update($this->admin, $this->ratingAdmin2));
        $this->assertEquals(false, $this->policy->update($this->admin, $this->ratingVendor2));
        $this->assertEquals(false, $this->policy->update($this->admin, $this->ratingCustomer2));

        $this->assertEquals(false, $this->policy->update($this->vendor, $this->ratingAdmin2));
        $this->assertEquals(false, $this->policy->update($this->vendor, $this->ratingVendor2));
        $this->assertEquals(false, $this->policy->update($this->vendor, $this->ratingCustomer2));

        $this->assertEquals(false, $this->policy->update($this->customer, $this->ratingAdmin2));
        $this->assertEquals(false, $this->policy->update($this->customer, $this->ratingVendor2));
        $this->assertEquals(false, $this->policy->update($this->customer, $this->ratingCustomer2));
    }

    /**
     * @covers App\Policies\RatingPolicy::delete
     * @group acl
     */
    public function testDeleteRating()
    {
        $this->createFields();

        $this->assertEquals(true, $this->policy->delete($this->admin, $this->ratingAdmin));
        $this->assertEquals(true, $this->policy->delete($this->vendor, $this->ratingVendor));
        $this->assertEquals(true, $this->policy->delete($this->customer, $this->ratingCustomer));

        $this->assertEquals(false, $this->policy->delete($this->admin, $this->ratingAdmin2));
        $this->assertEquals(false, $this->policy->delete($this->admin, $this->ratingVendor2));
        $this->assertEquals(false, $this->policy->delete($this->admin, $this->ratingCustomer2));

        $this->assertEquals(false, $this->policy->delete($this->vendor, $this->ratingAdmin2));
        $this->assertEquals(false, $this->policy->delete($this->vendor, $this->ratingVendor2));
        $this->assertEquals(false, $this->policy->delete($this->vendor, $this->ratingCustomer2));

        $this->assertEquals(false, $this->policy->delete($this->customer, $this->ratingAdmin2));
        $this->assertEquals(false, $this->policy->delete($this->customer, $this->ratingVendor2));
        $this->assertEquals(false, $this->policy->delete($this->customer, $this->ratingCustomer2));
    }

    protected function giveRatingToUser(User $user)
    {
        return Mockery::mock(Rating::class)
            ->makePartial()
            ->shouldReceive('getUserAttribute')
            ->andReturn($user)
            ->mock();
    }
}