<?php

namespace tests\Policies;

use App\Policies\UserPolicy;
use EG;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use TestCase;

class UserPolicyTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @before
     */
    public function createFields()
    {
        $this->policy = app(UserPolicy::class);
    }

    /**
     * @covers App\Policies\BasePolicy::before
     * @group acl
     */
    public function testInactiveUserAnyAction()
    {
        $this->assertNull($this->policy->before(EG::admin()));

        $inactive = EG::admin();
        $inactive->active = false;
        $this->assertEquals(false, $this->policy->before($inactive));
    }

    /**
     * @covers App\Policies\UserPolicy::view
     * @group acl
     */
    public function testViewUsers()
    {
        $this->assertEquals(true, $this->policy->view(EG::admin()));
        $this->assertEquals(true, $this->policy->view(EG::vendor()));
        $this->assertEquals(false, $this->policy->view(EG::customer()));
    }

    /**
     * @covers App\Policies\UserPolicy::viewDetails
     * @group acl
     */
    public function testViewUserDetails()
    {
        list($admin, $vendor, $customer) = [EG::admin(), EG::vendor(), EG::customer()];
        list($admin2, $vendor2, $customer2) = [EG::admin(), EG::vendor(), EG::customer()];

        // everyonce can see their own details
        $this->assertEquals(true, $this->policy->viewDetails($admin, $admin));
        $this->assertEquals(true, $this->policy->viewDetails($vendor, $vendor));
        $this->assertEquals(true, $this->policy->viewDetails($customer, $customer));

        // admin can see everything
        $this->assertEquals(true, $this->policy->viewDetails($admin, $admin2));
        $this->assertEquals(true, $this->policy->viewDetails($admin, $vendor2));
        $this->assertEquals(true, $this->policy->viewDetails($admin, $customer2));

        // vendors can see other vendors and customers
        $this->assertEquals(false, $this->policy->viewDetails($vendor, $admin2));
        $this->assertEquals(true, $this->policy->viewDetails($vendor, $vendor2));
        $this->assertEquals(true, $this->policy->viewDetails($vendor, $customer2));

        // customers can't see other users
        $this->assertEquals(false, $this->policy->viewDetails($customer, $admin2));
        $this->assertEquals(false, $this->policy->viewDetails($customer, $vendor2));
        $this->assertEquals(false, $this->policy->viewDetails($customer, $customer2));
    }

    /**
     * @covers App\Policies\UserPolicy::create
     * @group acl
     */
    public function testUserCreateUser()
    {
        list($admin, $vendor, $customer) = [EG::admin(), EG::vendor(), EG::customer()];
        list($admin2, $vendor2, $customer2) =
            [EG::usrAdminPL(), EG::usrVendorPL(), EG::usrCustomerPL()];

        $this->assertEquals(false, $this->policy->create($admin, $admin2));
        $this->assertEquals(true, $this->policy->create($admin, $vendor2));
        $this->assertEquals(true, $this->policy->create($admin, $customer2));

        $this->assertEquals(false, $this->policy->create($vendor, $admin2));
        $this->assertEquals(false, $this->policy->create($vendor, $vendor2));
        $this->assertEquals(true, $this->policy->create($vendor, $customer2));

        $this->assertEquals(false, $this->policy->create($customer, $admin2));
        $this->assertEquals(false, $this->policy->create($customer, $vendor2));
        $this->assertEquals(false, $this->policy->create($customer, $customer2));
    }

    /**
     * @covers App\Policies\UserPolicy::update
     * @group acl
     */
    public function testUserUpdateUser()
    {
        list($admin, $vendor, $customer) = [EG::admin(), EG::vendor(), EG::customer()];
        list($admin2, $vendor2, $customer2) = [EG::admin(), EG::vendor(), EG::customer()];

        $this->assertEquals(true, $this->policy->update($admin, $admin));
        $this->assertEquals(true, $this->policy->update($vendor, $vendor));
        $this->assertEquals(true, $this->policy->update($customer, $customer));

        $this->assertEquals(false, $this->policy->update($admin, $admin2));
        $this->assertEquals(true, $this->policy->update($admin, $vendor2));
        $this->assertEquals(true, $this->policy->update($admin, $customer2));

        $this->assertEquals(false, $this->policy->update($vendor, $admin2));
        $this->assertEquals(false, $this->policy->update($vendor, $vendor2));
        $this->assertEquals(true, $this->policy->update($vendor, $customer2));

        $this->assertEquals(false, $this->policy->update($customer, $admin2));
        $this->assertEquals(false, $this->policy->update($customer, $vendor2));
        $this->assertEquals(false, $this->policy->update($customer, $customer2));
    }

    /**
     * @covers App\Policies\UserPolicy::delete
     * @group acl
     */
    public function testUserDeleteUser()
    {
        list($admin, $vendor, $customer) = [EG::admin(), EG::vendor(), EG::customer()];
        list($admin2, $vendor2, $customer2) = [EG::admin(), EG::vendor(), EG::customer()];

        $this->assertEquals(false, $this->policy->delete($admin, $admin));
        $this->assertEquals(false, $this->policy->delete($vendor, $vendor));
        $this->assertEquals(false, $this->policy->delete($customer, $customer));

        $this->assertEquals(false, $this->policy->delete($admin, $admin2));
        $this->assertEquals(false, $this->policy->delete($admin, $vendor2));
        $this->assertEquals(false, $this->policy->delete($admin, $customer2));

        $this->assertEquals(false, $this->policy->delete($vendor, $admin2));
        $this->assertEquals(false, $this->policy->delete($vendor, $vendor2));
        $this->assertEquals(false, $this->policy->delete($vendor, $customer2));

        $this->assertEquals(false, $this->policy->delete($customer, $admin2));
        $this->assertEquals(false, $this->policy->delete($customer, $vendor2));
        $this->assertEquals(false, $this->policy->delete($customer, $customer2));
    }
}