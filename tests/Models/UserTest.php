<?php

namespace tests\Models;

use App\Models\PermissionLevel;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use TestCase;
use tests\Repositories\UserRepositoryTest;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Make sure active property and accessors work properly.
     *
     * @covers App\Models\User::getActiveAttribute
     * @covers App\Models\User::isActive
     * @covers App\Models\User::isInactive
     * @group datalayer
     */
    public function testGetActive()
    {
        $user = UserRepositoryTest::createTestAdmin();
        $this->assertEquals($user->active, $user->isActive());
        $this->assertTrue($user->isActive());
        $this->assertFalse($user->isInactive());
    }

    /**
     * Make sure inactive accessor works propery.
     *
     * @covers App\Models\User::isActive
     * @covers App\Models\User::isInactive
     * @group datalayer
     */
    public function testGetInactive()
    {
        $user = UserRepositoryTest::createTestAdmin();
        $user->active = false;
        $user->save();

        $this->assertFalse($user->isActive());
        $this->assertTrue($user->isInactive());
    }

    /**
     * Make sure permissions on an admin user are correct.
     *
     * @covers App\Models\User::isAdmin
     * @covers App\Models\User::isVendor
     * @covers App\Models\User::isCustomer
     * @group datalayer
     */
    public function testPermissionsOnAdmin()
    {
        $user = UserRepositoryTest::createTestAdmin();
        $this->assertEquals($user->permissionLevelType, PermissionLevel::ADMIN);
        $this->assertTrue($user->isAdmin());
        $this->assertFalse($user->isVendor());
        $this->assertFalse($user->isCustomer());
    }

    /**
     * Make sure permissions on a vendor user are correct.
     *
     * @covers App\Models\User::isAdmin
     * @covers App\Models\User::isVendor
     * @covers App\Models\User::isCustomer
     * @group datalayer
     */
    public function testPermissionsOnVendor()
    {
        $user = UserRepositoryTest::createTestVendor();
        $this->assertEquals($user->permissionLevelType, PermissionLevel::VENDOR);
        $this->assertFalse($user->isAdmin());
        $this->assertTrue($user->isVendor());
        $this->assertFalse($user->isCustomer());
    }

    /**
     * Make sure permissions on a customer user are correct.
     *
     * @covers App\Models\User::isAdmin
     * @covers App\Models\User::isVendor
     * @covers App\Models\User::isCustomer
     * @group datalayer
     */
    public function testPermissionsOnCustomer()
    {
        $user = UserRepositoryTest::createTestCustomer();
        $this->assertEquals($user->permissionLevelType, PermissionLevel::CUSTOMER);
        $this->assertFalse($user->isAdmin());
        $this->assertFalse($user->isVendor());
        $this->assertTrue($user->isCustomer());
    }

    /**
     * Test that same user as method works properly.
     *
     * @covers App\Models\User::isSameUserAs
     * @group datalayer
     */
    public function testUserSameAsAnotherUser()
    {
        $user1 = UserRepositoryTest::createTestCustomer();
        $user2 = UserRepositoryTest::createTestVendor();
        $this->assertTrue($user1->isSameUserAs($user1));
        $this->assertFalse($user1->isSameUserAs($user2));
    }

    /**
     * SCOPE TESTS
     */

    /**
     * @covers App\Models\User::scopeAdmins
     * @group datalayer
     */
    public function testScopeAdmins()
    {
        $collection = User::admins()->get();
        $remainder = $collection->reject(function ($user) {
            return $user->isVendor() or $user->isCustomer();
        });
        $this->assertEquals($collection->count(), $remainder->count());
    }

    /**
     * @covers App\Models\User::scopeVendors
     * @group datalayer
     */
    public function testScopeVendors()
    {
        $collection = User::vendors()->get();
        $remainder = $collection->reject(function ($user) {
            return $user->isAdmin() or $user->isCustomer();
        });
        $this->assertEquals($collection->count(), $remainder->count());
    }

    /**
     * @covers App\Models\User::scopeCustomers
     * @group datalayer
     */
    public function testScopeCustomers()
    {
        $collection = User::customers()->get();
        $remainder = $collection->reject(function ($user) {
            return $user->isAdmin() or $user->isVendor();
        });
        $this->assertEquals($collection->count(), $remainder->count());
    }
}
