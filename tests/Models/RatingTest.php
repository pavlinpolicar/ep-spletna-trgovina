<?php

namespace tests\Models;


use App\Repositories\RatingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use TestCase;
use tests\Controllers\ItemControllerTest;
use tests\Repositories\UserRepositoryTest;

class RatingTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Check that the user accessor works properly.
     *
     * @covers App\Models\Rating::getUserAttribute
     * @group datalayer
     */
    public function testUserAccessor()
    {
        $user = UserRepositoryTest::createTestCustomer();
        list(, $item) = ItemControllerTest::getVendorWithItem();

        $rating = RatingRepository::createRatingForItemByUser([
            'rating' => 8,
            'review' => 'This was good',
        ],
            $item,
            $user);

        $this->assertEquals($rating->user()->first()->id, $rating->user->id);
    }

    /**
     * Check that the item accessor works properly.
     *
     * @covers App\Models\Rating::getItemAttribute
     * @group datalayer
     */
    public function testItemAccessor()
    {
        $user = UserRepositoryTest::createTestCustomer();
        list(, $item) = ItemControllerTest::getVendorWithItem();

        $rating = RatingRepository::createRatingForItemByUser([
            'rating' => 8,
            'review' => 'This was good',
        ],
            $item,
            $user);

        $this->assertEquals($rating->item()->first()->id, $rating->item->id);
    }
}