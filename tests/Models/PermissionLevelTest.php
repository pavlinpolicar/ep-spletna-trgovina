<?php

use App\Models\PermissionLevel;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PermissionLevelTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that an admin permission level exists.
     *
     * @group datalayer
     */
    public function testAdminPermissionExists()
    {
        $permission = PermissionLevel::where('type', PermissionLevel::ADMIN)->first();
        $this->assertInstanceOf(PermissionLevel::class, $permission);
    }

    /**
     * Test that a vendor permission level exists.
     *
     * @group datalayer
     */
    public function testVendorPermissionExists()
    {
        $permission = PermissionLevel::where('type', PermissionLevel::VENDOR)->first();
        $this->assertInstanceOf(PermissionLevel::class, $permission);
    }

    /**
     * Test that a customer permission level exists.
     *
     * @group datalayer
     */
    public function testCustomerPermissionExists()
    {
        $permission = PermissionLevel::where('type', PermissionLevel::CUSTOMER)->first();
        $this->assertInstanceOf(PermissionLevel::class, $permission);
    }

    /**
     * Test that we can directly access the admin permission level instance.
     *
     * @covers App\Models\PermissionLevel::admin
     * @group datalayer
     */
    public function testDirectAdminAccessor()
    {
        $permission = PermissionLevel::admin();
        $this->assertInstanceOf(PermissionLevel::class, $permission);
        $this->assertEquals(PermissionLevel::ADMIN, $permission->type);
    }

    /**
     * Test that we can directly access the vendor permission level instance.
     *
     * @covers App\Models\PermissionLevel::vendor
     * @group datalayer
     */
    public function testDirectVendorAccessor()
    {
        $permission = PermissionLevel::vendor();
        $this->assertInstanceOf(PermissionLevel::class, $permission);
        $this->assertEquals(PermissionLevel::VENDOR, $permission->type);
    }

    /**
     * Test that we can directly access the customer permission level instance.
     *
     * @covers App\Models\PermissionLevel::customer
     * @group datalayer
     */
    public function testDirectCustomerAccessor()
    {
        $permission = PermissionLevel::customer();
        $this->assertInstanceOf(PermissionLevel::class, $permission);
        $this->assertEquals(PermissionLevel::CUSTOMER, $permission->type);
    }
}
