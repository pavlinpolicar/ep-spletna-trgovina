<?php

namespace tests\Controllers;

use App\Models\PermissionLevel;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use TestCase;
use tests\Repositories\UserRepositoryTest;

class UserControllerTest extends TestCase
{
    use DatabaseTransactions, WithoutMiddleware;

    /**
     * ADMIN USER TESTS
     */

    /**
     * Check that admin can see a list of all users.
     *
     * @group authorization
     */
    public function testViewAllUsersAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($admin)
            ->call('GET', route('api.users.index'));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that admin can see their own details.
     *
     * @group authorization
     */
    public function testViewOwnUserDetailsAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($admin)
            ->call('GET', route('api.users.retrieve', ['user' => $admin->id]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that admin can view a vendor user details.
     *
     * @group authorization
     */
    public function testViewVendorUserDetailsAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();
        $vendor = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($admin)
            ->call('GET', route('api.users.retrieve', ['user' => $vendor->id]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that admin can view a customer user details.
     *
     * @group authorization
     */
    public function testViewCustomerUserDetailsAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();
        $customer = UserRepositoryTest::createTestCustomer();

        $response = $this->actingAs($admin)
            ->call('GET', route('api.users.retrieve', ['user' => $customer->id]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that admin can't create another admin user.
     *
     * @group authorization
     */
    public function testCreateAdminUserAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();

        $newAdmin = self::createTestAdminData();
        $newAdmin['email'] = 'newadmin@store.local';

        $response = $this->actingAs($admin)
            ->call('POST', route('api.users.create'), $newAdmin);

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that admin can create a vendor user.
     *
     * @group authorization
     */
    public function testCreateVendorUserAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($admin)
            ->call('POST', route('api.users.create'), self::createTestVendorData());

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * Check that admin can create a customer user.
     *
     * @group authorization
     */
    public function testCreateCustomerUserAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($admin)
            ->call('POST', route('api.users.create'), self::createTestCustomerData());

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * Check that an admin can change their own data.
     *
     * @group authorization
     */
    public function testUpdateOwnUserDataAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($admin)
            ->call('PUT',
                route('api.users.update', ['user' => $admin->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that an admin can change a vendor user data.
     *
     * @group authorization
     */
    public function testUpdateVendorUserDataAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();
        $vendor = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($admin)
            ->call('PUT',
                route('api.users.update', ['user' => $vendor->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that an admin can change a customer user data.
     *
     * @group authorization
     */
    public function testUpdateCustomerUserDataAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();
        $customer = UserRepositoryTest::createTestCustomer();

        $response = $this->actingAs($admin)
            ->call('PUT',
                route('api.users.update', ['user' => $customer->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * VENDOR USER TESTS
     */

    /**
     * Check that vendor can see a list of all users.
     *
     * @group authorization
     */
    public function testViewAllUsersAsVendor()
    {
        $vendor = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($vendor)
            ->call('GET', route('api.users.index'));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that vendor can see their own details.
     *
     * @group authorization
     */
    public function testViewOwnUserDetailsAsVendor()
    {
        $vendor = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($vendor)
            ->call('GET', route('api.users.retrieve', ['user' => $vendor->id]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that vendor can view other vendor user details.
     *
     * @group authorization
     */
    public function testViewVendorUserDetailsAsVendor()
    {
        $vendor = UserRepositoryTest::createTestAdmin();
        $newVendor = UserRepositoryTest::createTestVendor();
        $newVendor['email'] = 'newVendor@store.local';

        $response = $this->actingAs($vendor)
            ->call('GET', route('api.users.retrieve', ['user' => $newVendor->id]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that vendor can view customer user details.
     *
     * @group authorization
     */
    public function testViewCustomerUserDetailsAsVendor()
    {
        $vendor = UserRepositoryTest::createTestAdmin();
        $customer = UserRepositoryTest::createTestCustomer();

        $response = $this->actingAs($vendor)
            ->call('GET', route('api.users.retrieve', ['user' => $customer->id]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that a vendor can't create an admin user.
     *
     * @group authorization
     */
    public function testCreateAdminUserAsVendor()
    {
        $vendor = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($vendor)
            ->call('POST', route('api.users.create'), self::createTestAdminData());

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a vendor can't create another vendor user.
     *
     * @group authorization
     */
    public function testCreateVendorUserAsVendor()
    {
        $vendor = UserRepositoryTest::createTestVendor();
        $newVendor = self::createTestVendorData();
        $newVendor['email'] = 'newVendor@store.local';

        $response = $this->actingAs($vendor)
            ->call('POST', route('api.users.create'), $newVendor);

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a vendor can create a customer user.
     *
     * @group authorization
     */
    public function testCreateCustomerUserAsVendor()
    {
        $vendor = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($vendor)
            ->call('POST', route('api.users.create'), self::createTestCustomerData());

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * Check that a vendor can change their own data.
     *
     * @group authorization
     */
    public function testUpdateOwnUserDataAsVendor()
    {
        $vendor = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($vendor)
            ->call('PUT',
                route('api.users.update', ['user' => $vendor->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that a vendor can't change admin user data.
     *
     * @group authorization
     */
    public function testUpdateAdminUserDataAsVendor()
    {
        $vendor = UserRepositoryTest::createTestVendor();
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($vendor)
            ->call('PUT',
                route('api.users.update', ['user' => $admin->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a vendor can't change other vendor user data.
     *
     * @group authorization
     */
    public function testUpdateVendorUserDataAsVendor()
    {
        $vendor = UserRepositoryTest::createTestVendor();
        $newVendorData = self::createTestVendorData();
        $newVendorData['email'] = 'newVendor@store.local';
        $newVendor = UserRepository::createVendor($newVendorData, $vendor);

        $response = $this->actingAs($vendor)
            ->call('PUT',
                route('api.users.update', ['user' => $newVendor->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a vendor can change customer user data.
     *
     * @group authorization
     */
    public function testUpdateCustomerUserDataAsVendor()
    {
        $vendor = UserRepositoryTest::createTestVendor();
        $customer = UserRepositoryTest::createTestCustomer();

        $response = $this->actingAs($vendor)
            ->call('PUT',
                route('api.users.update', ['user' => $customer->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * CUSTOMER USER TESTS
     */

    /**
     * Check that customer can't see a list of all users.
     *
     * @group authorization
     */
    public function testViewAllUsersAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();

        $response = $this->actingAs($customer)
            ->call('GET', route('api.users.index'));

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that customer can see their own details.
     *
     * @group authorization
     */
    public function testViewOwnUserDetailsAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();

        $response = $this->actingAs($customer)
            ->call('GET', route('api.users.retrieve', ['user' => $customer->id]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that customer can't view vendor user details.
     *
     * @group authorization
     */
    public function testViewAdminUserDetailsAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($customer)
            ->call('GET', route('api.users.retrieve', ['user' => $admin->id]));

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that customer can't view vendor user details.
     *
     * @group authorization
     */
    public function testViewVendorUserDetailsAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();
        $vendor = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($customer)
            ->call('GET', route('api.users.retrieve', ['user' => $vendor->id]));

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that customer can't view other customer user details.
     *
     * @group authorization
     */
    public function testViewCustomerUserDetailsAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();
        $newCustomerData = self::createTestCustomerData();
        $newCustomerData['email'] = 'newCustomer@store.local';
        $newCustomer = UserRepository::createCustomer($newCustomerData, $customer);

        $response = $this->actingAs($customer)
            ->call('GET', route('api.users.retrieve', ['user' => $newCustomer->id]));

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a customer can't create an admin user.
     *
     * @group authorization
     */
    public function testCreateAdminUserAsCustomer()
    {
        $customer = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($customer)
            ->call('POST', route('api.users.create'), self::createTestAdminData());

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a customer can't create a vendor user.
     *
     * @group authorization
     */
    public function testCreateVendorUserAsCustomer()
    {
        $customer = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($customer)
            ->call('POST', route('api.users.create'), self::createTestVendorData());

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a customer can't create other customer user.
     *
     * @group authorization
     */
    public function testCreateCustomerUserAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();
        $newCustomerData = self::createTestCustomerData();
        $newCustomerData['email'] = 'newCustomer@store.local';

        $response = $this->actingAs($customer)
            ->call('POST', route('api.users.create'), $newCustomerData);

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a customer can change their own data.
     *
     * @group authorization
     */
    public function testUpdateOwnUserDataAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();

        $response = $this->actingAs($customer)
            ->call('PUT',
                route('api.users.update', ['user' => $customer->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that a customer can't change admin user data.
     *
     * @group authorization
     */
    public function testUpdateAdminUserDataAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($customer)
            ->call('PUT',
                route('api.users.update', ['user' => $admin->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a customer can't change other vendor user data.
     *
     * @group authorization
     */
    public function testUpdateVendorUserDataAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();
        $vendor = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($customer)
            ->call('PUT',
                route('api.users.update', ['user' => $vendor->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a customer can't change other customer user data.
     *
     * @group authorization
     */
    public function testUpdateCustomerUserDataAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();
        $newCustomerData = self::createTestCustomerData();
        $newCustomerData['email'] = 'newCustomer@store.local';
        $newCustomer = UserRepository::createCustomer($newCustomerData, $customer);

        $response = $this->actingAs($customer)
            ->call('PUT',
                route('api.users.update', ['user' => $newCustomer->id]),
                self::simpleUserUpdateRequestData());

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * HELPER METHODS
     */

    /**
     * Get sample request data for creating an admin user.
     *
     * @return array
     */
    public static function createTestAdminData()
    {
        return array_merge(self::createTestUserData(),
            [
                'permission_level' => PermissionLevel::ADMIN,
            ]);
    }

    /**
     * Get sample request data for creating a vendor user.
     *
     * @return array
     */
    public static function createTestVendorData()
    {
        return array_merge(self::createTestUserData(),
            [
                'permission_level' => PermissionLevel::VENDOR,
            ]);
    }

    /**
     * Get sample request data for creating a customer user.
     *
     * @return array
     */
    public static function createTestCustomerData()
    {
        return array_merge(self::createTestUserData(),
            [
                'permission_level' => PermissionLevel::CUSTOMER,
                'address' => 'Dreary lane',
                'telephone' => '000-111-222',
            ]);
    }

    /**
     * Sample user data.
     *
     * @return array
     */
    protected static function createTestUserData()
    {
        return [
            'name' => 'Jane',
            'surname' => 'Doe',
            'email' => 'jane.doe@store.local',
            'password' => 'password',
            'confirm_password' => 'password',
        ];
    }

    /**
     * Get sample request data for updating an exising user.
     *
     * @return array
     */
    protected static function simpleUserUpdateRequestData()
    {
        return [
            'name' => 'John',
            'surname' => 'Smith',
        ];
    }
}
