<?php

namespace tests\Controllers;


use App\Models\Item;
use App\Models\User;
use App\Repositories\ItemRepository;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use TestCase;
use tests\Repositories\UserRepositoryTest;

class ItemControllerTest extends TestCase
{
    use DatabaseTransactions, WithoutMiddleware;

    /**
     * Anyone can get a list of all the items.
     *
     * @group authorization
     */
    public function testViewAllItems()
    {
        $response = $this->call('GET', route('api.items.index'));
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Anyone can get item details.
     *
     * @group authorization
     */
    public function testViewItemDetails()
    {
        $item = Item::first();
        $response = $this->call('GET', route('api.items.retrieve', ['item' => $item->id]));
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * ADMIN USER TESTS
     */

    /**
     * Check that an administrator can't add items to sell.
     *
     * @group authorization
     */
    public function testCreateItemAsAdmin()
    {
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($admin)
            ->call('POST', route('api.items.create'), self::createSimpleItemData());

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that an admin can't edit items, since they can't sell them either.
     *
     * @group authorization
     */
    public function testUpdateItemAsAdmin()
    {
        list($vendor, $item) = self::getVendorWithItem();
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($admin)
            ->call('PUT', route('api.items.update', ['item' => $item->id]));

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that an admin can't delete items.
     *
     * @group authorization
     */
    public function testDeleteItemAsAdmin()
    {
        list($vendor, $item) = self::getVendorWithItem();
        $admin = UserRepositoryTest::createTestAdmin();

        $response = $this->actingAs($admin)
            ->call('DELETE', route('api.items.delete', ['item' => $item->id]));

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * VENDOR USER TESTS
     */

    /**
     * Check that an administrator can't add items to sell.
     *
     * @group authorization
     */
    public function testCreateItemAsVendor()
    {
        $vendor = UserRepositoryTest::createTestVendor();

        $response = $this->actingAs($vendor)
            ->call('POST', route('api.items.create'), self::createSimpleItemData());

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * Check that a vendor can edit their own items.
     *
     * @group authorization
     */
    public function testUpdateOwnItemAsVendor()
    {
        list($vendor, $item) = self::getVendorWithItem();

        $response = $this->actingAs($vendor)
            ->call('PUT', route('api.items.update', ['item' => $item->id]));

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Check that a vendor can't edit other vendors items.
     *
     * @group authorization
     */
    public function testUpdateOtherItemsAsVendor()
    {
        list($vendor, $item) = self::getVendorWithItem();
        $newVendorData = UserControllerTest::createTestVendorData();
        $newVendorData['email'] = 'newVendor@store.local';
        $newVendor = UserRepository::createVendor($newVendorData, $vendor);

        $response = $this->actingAs($newVendor)
            ->call('PUT', route('api.items.update', ['item' => $item->id]));

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a vendor can delete their own item.
     *
     * @group authorization
     */
    public function testDeleteOwnItemAsVendor()
    {
        list($vendor, $item) = self::getVendorWithItem();

        $response = $this->actingAs($vendor)
            ->call('DELETE', route('api.items.delete', ['item' => $item->id]));

        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    /**
     * Check that a vendor can't delete other vendors item.
     *
     * @group authorization
     */
    public function testDeleteOtherItemsAsVendor()
    {
        list($vendor, $item) = self::getVendorWithItem();
        $newVendorData = UserControllerTest::createTestVendorData();
        $newVendorData['email'] = 'newVendor@store.local';
        $newVendor = UserRepository::createVendor($newVendorData, $vendor);

        $response = $this->actingAs($newVendor)
            ->call('DELETE', route('api.items.delete', ['item' => $item->id]));

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * CUSTOMER USER TEST
     */

    /**
     * Check that a customer can't add items to sell.
     *
     * @group authorization
     */
    public function testCreateItemAsCustomer()
    {
        $customer = UserRepositoryTest::createTestCustomer();

        $response = $this->actingAs($customer)
            ->call('POST', route('api.items.create'), self::createSimpleItemData());

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a customer can't edit vendor items.
     *
     * @group authorization
     */
    public function testUpdateOwnItemAsCustomer()
    {
        list($vendor, $item) = self::getVendorWithItem();
        $customer = UserRepositoryTest::createTestCustomer();

        $response = $this->actingAs($customer)
            ->call('PUT', route('api.items.update', ['item' => $item->id]));

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Check that a customer can't delete items.
     *
     * @group authorization
     */
    public function testDeleteItemAsCustomer()
    {
        list($vendor, $item) = self::getVendorWithItem();
        $customer = UserRepositoryTest::createTestCustomer();

        $response = $this->actingAs($customer)
            ->call('DELETE', route('api.items.delete', ['item' => $item->id]));

        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * HELPER METHODS
     */

    /**
     * Get sample item data.
     *
     * @return array
     */
    protected static function createSimpleItemData()
    {
        return [
            'display' => 'Tooth brush',
            'desc' => 'Sample description',
            'price' => 20.5,
        ];
    }

    /**
     * Get a vendor with one item.
     *
     * @return User
     */
    public static function getVendorWithItem()
    {
        $vendor = UserRepositoryTest::createTestVendor();
        $item = ItemRepository::createItem(self::createSimpleItemData(), $vendor);
        return [$vendor, $item];
    }
}