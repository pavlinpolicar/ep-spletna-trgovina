<?php

namespace Acceptance;

use Symfony\Component\HttpFoundation\Response;
use TestCase;

class Items extends TestCase
{
    /**
     * @covers App\Http\Api\V1\Controllers\ItemController::index
     * @group acceptance
     */
    public function testViewAllItemsPage()
    {
        $this->get(route('api.items.index'))->seeJson()
            ->assertResponseStatus(Response::HTTP_OK);
    }

    /**
     * @covers App\Http\Api\V1\Controllers\ItemController::index
     * @group acceptance
     */
    public function testViewAllItemsPaginated()
    {
        $paginate = 2;
        $request = $this->get(route('api.items.index', ['pageSize' => $paginate]));

        $request->seeJson()
            ->assertResponseStatus(Response::HTTP_OK);

        $body = json_decode($request->response->content(), true);
        $this->assertCount($paginate, $body['data']);
    }
}