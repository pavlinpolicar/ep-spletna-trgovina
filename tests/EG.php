<?php

use App\Models\PermissionLevel;
use App\Models\User;

class EG
{
    public static function admin()
    {
        $user = factory(User::class, 'admin')->create();
        return User::find($user->id);
    }

    public static function vendor()
    {
        $user = factory(User::class, 'vendor')->create();
        return User::find($user->id);
    }

    public static function customer()
    {
        $user = factory(User::class, 'customer')->create();
        return User::find($user->id);
    }

    public static function usrAdminPL()
    {
        $user = new User();
        $user->permissionLevelType = PermissionLevel::ADMIN;
        return $user;
    }

    public static function usrVendorPL()
    {
        $user = new User();
        $user->permissionLevelType = PermissionLevel::VENDOR;
        return $user;
    }

    public static function usrCustomerPL()
    {
        $user = new User();
        $user->permissionLevelType = PermissionLevel::CUSTOMER;
        return $user;
    }
}