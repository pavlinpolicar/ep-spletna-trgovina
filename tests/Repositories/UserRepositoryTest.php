<?php

namespace tests\Repositories;


use App\Models\PermissionLevel;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use TestCase;

class UserRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test creating a user with fields
     *
     * @covers App\Repositories\UserRepository::createUserWithFields
     * @covers App\Repositories\UserRepository::createUserWithPermission
     * @group datalayer
     */
    public function testCreateUserWithFields()
    {
        $user = self::createTestAdmin();
        list($name, $surname, $email, $password, $permissionLevel) =
            ['Jane', 'Doe', 'jane.doe@store.local', 'password', PermissionLevel::CUSTOMER];
        $newUser = UserRepository::createUserWithFields([
            'name' => $name,
            'surname' => $surname,
            'email' => $email,
            'password' => Hash::make($password),
            'permission_level' => $permissionLevel,
        ], $user);

        $this->assertInstanceOf(User::class, $newUser);
        $this->assertNotNull($newUser->id);

        $this->assertInstanceOf(User::class, $newUser->createdBy()->first());
        $this->assertTrue($user->isSameUserAs($newUser->createdBy()->first()));
    }

    /**
     * Test creating an admin user.
     *
     * @covers App\Repositories\UserRepository::createAdmin
     * @group datalayer
     */
    public function testCreateAdminUser()
    {
        $user = self::createTestAdmin();
        $this->assertEquals($user->permissionLevelType, PermissionLevel::ADMIN);
    }

    /**
     * Test creating a vendor user.
     *
     * @covers App\Repositories\UserRepository::createVendor
     * @group datalayer
     */
    public function testCreateVendorUser()
    {
        $user = self::createTestVendor();
        $this->assertEquals($user->permissionLevelType, PermissionLevel::VENDOR);
    }

    /**
     * Test creating a customer user.
     *
     * @covers App\Repositories\UserRepository::createCustomer
     * @group datalayer
     */
    public function testCreateCustomerUser()
    {
        $user = self::createTestCustomer();
        $this->assertEquals($user->permissionLevelType, PermissionLevel::CUSTOMER);
    }

    /**
     * Create a simple admin user.
     *
     * @return User
     */
    public static function createTestAdmin()
    {
        return UserRepository::createAdmin([
            'name' => 'admin',
            'surname' => '',
            'email' => 'admin@store.local',
            'password' => Hash::make('password'),
        ], User::find(1));
    }

    /**
     * Create a simple vendor user.
     *
     * @return User
     */
    public static function createTestVendor()
    {
        return UserRepository::createVendor([
            'name' => 'vendor',
            'surname' => '',
            'email' => 'vendor@store.local',
            'password' => Hash::make('password'),
        ], User::find(1));
    }

    /**
     * Create a simple customer user.
     *
     * @return User
     */
    public static function createTestCustomer()
    {
        return UserRepository::createCustomer([
            'name' => 'customer',
            'surname' => '',
            'email' => 'customer@store.local',
            'password' => Hash::make('password'),
        ], User::find(1));
    }
}