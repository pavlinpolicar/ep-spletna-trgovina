<?php

namespace tests\Repositories;

use App\Repositories\CartRepository;
use App\Repositories\ItemRepository;
use EG;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use TestCase;

class CartRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @covers App\Repositories\CartRepository::userHasItemInCart
     * @group datalayer
     */
    public function testUserHasItemInCart()
    {
        list(, $item, $customer,) = $this->getTestData();

        // assert that user doesn't have the item in cart before we put it there
        $this->assertFalse(CartRepository::userHasItemInCart($customer, $item));

        $customer->cart()->attach($item);

        // assert that user has item in cart after we put it there
        $this->assertTrue(CartRepository::userHasItemInCart($customer, $item));
    }

    /**
     * @covers App\Repositories\CartRepository::userHasItemsInCart
     * @group datalayer
     */
    public function testUserHasItemsInCart()
    {
        list(, $items, $customer) = $this->getTestDataMultipleItems(5);

        $itemsInCart = Collection::make([$items[0], $items[2], $items[4]]);

        $this->assertFalse(CartRepository::userHasItemsInCart($customer, $itemsInCart));

        foreach ($itemsInCart as $item) {
            CartRepository::addItemToUserCart($item, $customer, 1);
        }

        $this->assertTrue(CartRepository::userHasItemsInCart($customer, $itemsInCart));
    }

    /**
     * @covers App\Repositories\CartRepository::addItemToUserCart
     * @group datalayer
     */
    public function testAddItemToUserCart()
    {
        list(, $item, $customer, $quantity) = $this->getTestData();

        CartRepository::addItemToUserCart($item, $customer, $quantity);

        // check that the user has the item in their cart
        $this->assertEquals($item->id, $customer->cart()->first()->id);

        // check that the quantity was input properly
        $this->assertEquals($quantity, $customer->cart()->first()->pivot->quantity);

        // check that the inverse relationship was input properly
        $this->assertEquals($item->userCarts()->first()->id, $customer->id);
    }

    /**
     * @covers App\Repositories\CartRepository::addItemToUserCart
     * @group datalayer
     * @expectedException \App\Exceptions\ItemAlreadyInCartException
     */
    public function testAddItemToCartFailsOnItemThatCartAlreadyContains()
    {
        list(, $item, $customer, $quantity) = $this->getTestData();

        CartRepository::addItemToUserCart($item, $customer, $quantity);
        CartRepository::addItemToUserCart($item, $customer, $quantity);
    }


    /**
     * @covers App\Repositories\CartRepository::updateItemQuantityInCart
     * @group datalayer
     */
    public function testUpdateItemQuantityInUserCart()
    {
        $newQuantity = 34;

        list(, $item, $customer, $quantity) = $this->getTestData();
        CartRepository::addItemToUserCart($item, $customer, $quantity);

        CartRepository::updateItemQuantityInCart($item, $customer, $newQuantity);

        $this->assertEquals($newQuantity, $item->userCarts()->first()->pivot->quantity);
    }

    /**
     * @covers App\Repositories\CartRepository::updateItemQuantityInCart
     * @group datalayer
     * @expectedException \App\Exceptions\ItemNotInCartException
     */
    public function testUpdateItemQuantityInUserCartFailsWhenItemNotInCart()
    {
        $newQuantity = 34;

        list(, $item, $customer,) = $this->getTestData();

        CartRepository::updateItemQuantityInCart($item, $customer, $newQuantity);
    }

    /**
     * @covers App\Repositories\CartRepository::removeItemFromCart
     * @group datalayer
     */
    public function testRemoveItemFromUsersCart()
    {
        list(, $item, $customer, $quantity) = $this->getTestData();

        CartRepository::addItemToUserCart($item, $customer, $quantity);

        $this->assertTrue(CartRepository::userHasItemInCart($customer, $item));
        CartRepository::removeItemFromCart($item, $customer);
        $this->assertFalse(CartRepository::userHasItemInCart($customer, $item));
    }

    protected function getTestData()
    {
        $vendor = EG::vendor();
        $item = ItemRepository::createItem([
            'display' => 'item',
            'price' => 20.44,
        ],
            $vendor);

        $customer = EG::customer();
        $quantity = 1;

        return [$vendor, $item, $customer, $quantity];
    }

    protected function getTestDataMultipleItems($numItems = 1)
    {
        $vendor = EG::vendor();
        $items = [];
        for ($i = 0; $i < $numItems; ++$i) {
            $items[] = ItemRepository::createItem([
                'display' => 'item' . $i,
                'price' => 5.0 * $i
            ],
                $vendor);
        }
        $customer = EG::customer();

        return [$vendor, $items, $customer];
    }

}