<?php

namespace tests\Repositories;


use App\Models\Item;
use App\Models\User;
use App\Repositories\ItemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use TestCase;

class ItemRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that an item gets created properly.
     *
     * @covers App\Repositories\ItemRepository::createItem
     * @group datalayer
     */
    public function testCreateItem()
    {
        $vendor = UserRepositoryTest::createTestVendor();

        list($display, $description, $price) = ['Tooth brush', 'Keep your teeth clean.', 25.23];

        $item = ItemRepository::createItem([
            'display' => $display,
            'description' => $description,
            'price' => $price,
        ], $vendor);

        $this->assertInstanceOf(Item::class, $item);
        $this->assertNotNull($item->id);

        $this->assertInstanceOf(User::class, $item->vendor);
        $this->assertTrue($vendor->isSameUserAs($item->vendor));

        $this->assertEquals($display, $item->display);
        $this->assertEquals($description, $item->description);
        $this->assertEquals($price, $item->price);
    }
}