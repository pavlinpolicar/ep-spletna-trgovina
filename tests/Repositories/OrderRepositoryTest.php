<?php

namespace Repositories;


use App\Models\Order;
use App\Models\OrderGroup;
use App\Repositories\OrderRepository;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;
use TestCase;
use tests\Repositories\UserRepositoryTest;

class OrderRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @covers App\Repositories\OrderRepository::createOrderGroupFromArrayForUser
     * @group datalayer
     */
    public function testcreateOrderGroupFromArrayForUser()
    {
        $customer = UserRepositoryTest::createTestCustomer();

    }

    public function testCreateOrder()
    {
        $group = $this->getOrderGroup();
        $order = OrderRepository::createOrder([
            'item_id' => 1,
            'quantity' => 1,
        ],
            $group);

        $this->assertInstanceOf(Order::class, $order);
    }

    protected function getOrderGroup()
    {
        $ordersRelationship = Mockery::mock(HasMany::class)
            ->shouldReceive('save')
            ->withAnyArgs()
            ->andReturn($this->returnArgument(0))
            ->getMock();

        return Mockery::mock(OrderGroup::class)
            ->shouldReceive('orders')
            ->andReturn($ordersRelationship)
            ->set('id', 1)
            ->getMock();
    }
}