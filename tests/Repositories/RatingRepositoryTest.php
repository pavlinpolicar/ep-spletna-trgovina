<?php

namespace tests\Repositories;


use App\Models\Item;
use App\Models\Rating;
use App\Models\User;
use App\Repositories\RatingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use TestCase;
use tests\Controllers\ItemControllerTest;

class RatingRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that a rating gets created properly.
     *
     * @covers App\Repositories\RatingRepository::createRatingForItemByUser
     * @group datalayer
     */
    public function testCreateRating()
    {
        list($ratingScore, $review) = [8, 'This was good'];

        $user = UserRepositoryTest::createTestCustomer();
        list(, $item) = ItemControllerTest::getVendorWithItem();

        $rating = RatingRepository::createRatingForItemByUser([
            'rating' => $ratingScore,
            'review' => $review,
        ],
            $item,
            $user);

        // check that the object was created correctly
        $this->assertInstanceOf(Rating::class, $rating);
        $this->assertNotNull($rating->id);

        // check that the properties were saved correctly
        $this->assertEquals($ratingScore, $rating->rating);
        $this->assertEquals($review, $rating->review);

        // check that the item was linked correctly
        $this->assertInstanceOf(Item::class, $rating->item);
        $this->assertEquals($item->id, $rating->item->id);

        // check that the user was linked correctly
        $this->assertInstanceOf(User::class, $rating->user);
        $this->assertEquals($user->id, $rating->user->id);

        // check links from other direction
        $this->assertEquals($item->ratings()->first()->id, $rating->id);
        $this->assertEquals($user->ratings()->first()->id, $rating->id);
    }
}