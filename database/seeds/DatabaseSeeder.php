<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(PermissionLevelsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ItemsTableSeeder::class);
        $this->call(CartItemsTableSeeder::class);
        $this->call(UserLogEntriesTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(RatingsTableSeeder::class);

        Model::reguard();
    }
}
