<?php

use App\Models\PermissionLevel;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear the table before seeding
        DB::table('users')->delete();

        $adminPermission = PermissionLevel::where('type', PermissionLevel::ADMIN)->first();
        $adminPermission->users()->saveMany([
            self::createUser('Admin', ''),
        ]);

        $vendorPermission = PermissionLevel::where('type', PermissionLevel::VENDOR)->first();
        $vendorPermission->users()->saveMany([
            self::createUser('Branko', 'Novak'),
            self::createUser('Stanka', 'Kovač'),
        ]);

        $customerPermission = PermissionLevel::where('type', PermissionLevel::CUSTOMER)->first();
        $customerPermission->users()->saveMany([
            self::createUser('Janko', 'Bohorič'),
            self::createUser('Metka', 'Bohorič'),
            self::createUser('Janez', 'Klemenčič'),
            self::createUser('Karl', 'Belin'),
        ]);
    }

    /**
     * Create a dummy user with credidentials using a given name and surname.
     *
     * @param $name
     * @param $surname
     * @return User
     */
    private static function createUser($name, $surname)
    {
        return new User([
            'name' => $name,
            'surname' => $surname,
            'email' => strtolower($name . '.' . $surname . '@store.local'),
            'password' => Hash::make('password')
        ]);
    }
}
