<?php

use App\Models\Item;
use App\Models\User;
use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear the table before seeding
        DB::table('items')->delete();

        $branko = User::where('name', 'Branko')->first();
        $branko->items()->saveMany([
            new Item([
                'display' => 'Tooth brush',
                'description' => 'A necessity for everyone.',
                'price' => 3.54
            ]),
            new Item([
                'display' => 'Yoghurt',
                'description' => 'Just yoghurt.',
                'price' => 0.88
            ]),
            new Item([
                'display' => 'Pack of paper',
                'description' => 'Need I say more?',
                'price' => 5.37
            ]),
            new Item([
                'display' => 'Kinder egg',
                'description' => 'Banned in the USA, get it where you can!',
                'price' => 0.99
            ]),
            new Item([
                'display' => 'Coolermaster X-lite II',
                'description' => 'A cooler for your laptop.',
                'price' => 23.77
            ]),
            new Item([
                'display' => 'Samsung Galaxy Ace',
                'description' => 'A shitty phone.',
                'price' => 230.25
            ]),
            new Item([
                'display' => 'Lenovo IdeaPad Y50-70',
                'description' => 'A wannabe Lenovo laptop that suffers severely from overheating.',
                'price' => 899.98
            ]),
            new Item([
                'display' => 'Nikon Coolpix L840',
                'description' => '',
                'price' => 199.99
            ]),
            new Item([
                'display' => 'BGL35110 BOSCH',
                'description' => 'An overpriced vacum cleaner.',
                'price' => 116.79
            ]),
            new Item([
                'display' => 'Roomba 886 Vacum robot irobot',
                'description' => 'It goes wroom wroom.',
                'price' => 799.99
            ])
        ]);

        $stanka = User::where('name', 'Stanka')->first();
        $stanka->items()->saveMany([
            new Item([
                'display' => 'HX6721/35 Healthy White 3 Sonicare',
                'description' => 'It may not seem like it, but it\'s just a toothbrush',
                'price' => 112.99
            ]),
            new Item([
                'display' => 'Trust 19367 TYTAN 2.1 Bluetooth Speakers',
                'description' => '',
                'price' => 112.99
            ]),
            new Item([
                'display' => 'SD 16GB Ultra 80MB/S SanDisk',
                'description' => '',
                'price' => 12.99
            ]),
            new Item([
                'display' => 'Micro SD 32GB Extra 90MB/S SanDisk',
                'description' => '',
                'price' => 29.99
            ]),
            new Item([
                'display' => 'Guitar EST11 BK Flight Black Electric Guitar',
                'description' => 'Not really an instrument.',
                'price' => 99.99
            ])
        ]);
    }
}
