<?php

use App\Models\Item;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Database\Seeder;

class RatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear the tables before seeding
        DB::table('ratings')->delete();

        // Janko likes things
        $janko = User::where('name', 'Janko')->first();
        $janko->ratings()->saveMany([
            self::createRating(1, 10, 'Thanks! This was great.'),
            self::createRating(3, 8),
            self::createRating(4, 9, 'This helped me a lot.'),
            self::createRating(10, 9),
            self::createRating(11, 9),
        ]);

        // Metka is one close fisted fucker
        $metka = User::where('name', 'Metka')->first();
        $metka->ratings()->saveMany([
            self::createRating(1, 7),
            self::createRating(3, 5, 'What a waste of money.'),
            self::createRating(4, 6),
            self::createRating(5, 5),
            self::createRating(14, 3, 'What a complete piece of shit.'),
        ]);

        // Janez is more realistic I guess...
        $janez = User::where('name', 'Janez')->first();
        $janez->ratings()->saveMany([
            self::createRating(1, 2, 'This was not helpful.'),
            self::createRating(6, 10),
            self::createRating(4, 9),
        ]);

    }

    /**
     * Create a dummy rating object.
     *
     * @param $itemId
     * @param $rating
     * @param string $review
     * @return Rating
     */
    private static function createRating($itemId, $rating, $review = '')
    {
        return new Rating([
            'item_id' => $itemId,
            'rating' => $rating,
            'review' => $review,
        ]);
    }
}
