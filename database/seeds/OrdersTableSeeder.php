<?php

use App\Models\Item;
use App\Models\Order;
use App\Models\OrderGroup;
use App\Models\User;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear the tables before seeding
        DB::table('order_groups')->delete();
        DB::table('orders')->delete();

        $janko = User::where('name', 'Janko')->first();
        self::createOrderGroup($janko, [[1, 1], [2, 1], [11, 3], [9, 1]]);
        self::createOrderGroup($janko, [[13, 1]]);

        $metka = User::where('name', 'Metka')->first();
        self::createOrderGroup($metka, [[11, 2], [10, 1]]);
        self::createOrderGroup($metka, [[1, 1], [2, 3]]);

        $janez = User::where('name', 'Janez')->first();
        self::createOrderGroup($janez, [[11, 1], [13, 1]]);
        self::createOrderGroup($janez, [[5, 1], [3, 1], [10, 2]]);
        self::createOrderGroup($janez, [[6, 1], [7, 1]]);
        self::createOrderGroup($janez, [[9, 1]]);
    }

    /**
     * Correctly create an order group with given user and orders.
     *
     * @param $user
     * @param $orders
     */
    private static function createOrderGroup($user, $orders)
    {
        $group = new OrderGroup();
        $user->orderGroups()->save($group);
        $group->orders()->saveMany(self::createOrders($orders));
    }

    /**
     * Create an array of attached (properly associated with the item) order objects with a given
     * array of item ids.
     *
     * @param $orders Array of arrays, first index is item id, second index is order quanitity.
     * @return array
     */
    private static function createOrders($orders)
    {
        if (!is_array($orders[0])) {
            return self::createOrders([$orders]);
        }

        return array_map(function ($order) {
            return new Order([
                'item_id' => $order[0],
                'quantity' => $order[1]
            ]);
        }, $orders);
    }
}
