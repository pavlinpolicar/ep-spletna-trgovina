<?php

use App\Models\PermissionLevel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear the table before seeding
        DB::table('permission_levels')->delete();

        DB::table('permission_levels')->insert([
            'type' => PermissionLevel::ADMIN,
            'display' => 'Administrator',
            'description' => 'A website administrator.'
        ]);

        DB::table('permission_levels')->insert([
            'type' => PermissionLevel::VENDOR,
            'display' => 'Vendor',
            'description' => 'A party interested in selling items on this web store.'
        ]);

        DB::table('permission_levels')->insert([
            'type' => PermissionLevel::CUSTOMER,
            'display' => 'Customer',
            'description' => 'A party interested in buying items on this web store.'
        ]);
    }
}
