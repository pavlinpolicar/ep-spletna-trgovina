<?php

use App\Models\User;
use App\Models\UserLogEntry;
use Illuminate\Database\Seeder;

class UserLogEntriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear the table before seeding
        DB::table('user_log_entries')->delete();

        // we only need to log admin and vendor
        $admin = User::where('name', 'Admin')->first();
        $admin->logEntries()->saveMany([
            self::createUserLogEntry('Did some stuff.'),
            self::createUserLogEntry('Did some more stuff.'),
            self::createUserLogEntry('Did some boring stuff.'),
            self::createUserLogEntry('Did some interesting stuff.'),
            self::createUserLogEntry('Got bored and left.'),
        ]);

        $branko = User::where('name', 'Branko')->first();
        $branko->logEntries()->saveMany([
            self::createUserLogEntry('Added an item to sell.'),
            self::createUserLogEntry('Looked at Stanka\'s picture. Naughty Branko.'),
            self::createUserLogEntry('Reviewed their history.'),
            self::createUserLogEntry('Did some stuff.'),
            self::createUserLogEntry('Did some more stuff.'),
            self::createUserLogEntry('Got bored and left.'),
        ]);

        $stanka = User::where('name', 'Stanka')->first();
        $stanka->logEntries()->saveMany([
            self::createUserLogEntry('Reviewed their history.'),
            self::createUserLogEntry('Did some stuff.'),
            self::createUserLogEntry('Did some more stuff.'),
            self::createUserLogEntry('Got bored and left.'),
        ]);
    }

    /**
     * Create a dummy user log entry.
     *
     * @param $description
     * @return UserLogEntry
     */
    private static function createUserLogEntry($description)
    {
        return new UserLogEntry([
            'description' => $description,
        ]);
    }
}
