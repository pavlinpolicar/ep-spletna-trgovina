<?php

use App\Models\Item;
use App\Models\User;
use Illuminate\Database\Seeder;

class CartItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear the table before seeding
        DB::table('cart_items')->delete();

        $itemList = Item::all();

        // Janko likes to dream big
        $janko = User::where('name', 'Janko')->first();
        $janko->cart()->save($itemList[2], ['quantity' => 1]);
        $janko->cart()->save($itemList[3], ['quantity' => 1]);
        $janko->cart()->save($itemList[4], ['quantity' => 3]);
        $janko->cart()->save($itemList[13], ['quantity' => 1]);
        $janko->cart()->save($itemList[12], ['quantity' => 1]);

        // Metka... not so much
        $metka = User::where('name', 'Metka')->first();
        $metka->cart()->save($itemList[14], ['quantity' => 2]);
        $metka->cart()->save($itemList[0], ['quantity' => 1]);
    }
}
