<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Item;
use App\Models\PermissionLevel;
use App\Models\User;

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'active' => true,
    ];
});

$factory->defineAs(User::class, 'admin', function () use ($factory) {
    $user = $factory->raw(User::class);
    return array_merge($user, [
        'permission_level_id' => PermissionLevel::admin()->id,
    ]);
});

$factory->defineAs(User::class, 'vendor', function () use ($factory) {
    $user = $factory->raw(User::class);
    return array_merge($user, [
        'permission_level_id' => PermissionLevel::vendor()->id,
    ]);
});

$factory->defineAs(User::class, 'customer', function () use ($factory) {
    $user = $factory->raw(User::class);
    return array_merge($user, [
        'permission_level_id' => PermissionLevel::customer()->id,
    ]);
});

$factory->define(Item::class, function (Faker\Generator $faker) {
    // TODO change the factory to actually return a valid item and not just some random word.
    return [
        'display' => $faker->word,
        'description' => $faker->sentence(),
        'price' => 20.0,
    ];
});