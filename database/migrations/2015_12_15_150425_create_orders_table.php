<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantity')->unsigned();
            $table->enum('status', ['PENDING', 'DELIVERED', 'CANCELLED'])->default('PENDING');

            // Foreign keys
            $table->integer('order_group_id')->unsigned();
            $table->integer('item_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::drop('orders');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
