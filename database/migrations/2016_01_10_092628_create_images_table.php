<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->string('name');
            $table->timestamps();

//            $table->foreign('item_id')
//                ->references('id')
//                ->on('items')
//                ->onUpdate('cascade')
//                ->onDelete('cascade');
        });

        Schema::table('items', function (Blueprint $table) {
            $table->integer('display_image_id')->nullable()->default(null);
//            $table->foreign('display_image_id')
//                ->references('id')
//                ->on('images')
//                ->onUpdate('cascade')
//                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('images');

        Schema::table('items', function (Blueprint $table) {
//            $table->dropForeign('display_image_id');
            $table->dropColumn('display_image_id');
        });
    }
}
