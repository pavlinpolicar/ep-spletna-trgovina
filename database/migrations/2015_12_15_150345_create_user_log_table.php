<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_log_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');

            // Foreign keys
            $table->integer('user_id')->unsigned();

            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::drop('user_log_entries');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
