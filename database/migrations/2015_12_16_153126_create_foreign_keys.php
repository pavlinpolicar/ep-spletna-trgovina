<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Users table
        Schema::table('users', function(Blueprint $table) {
            $table->foreign('permission_level_id')
                ->references('id')
                ->on('permission_levels')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // Items table
        Schema::table('items', function(Blueprint $table) {
            $table->foreign('vendor_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // Ratings table
        Schema::table('ratings', function(Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
//            $table->foreign('item_id')
//                ->references('id')
//                ->on('items')
//                ->onUpdate('cascade')
//                ->onDelete('cascade');
        });

        // User log entries table
        Schema::table('user_log_entries', function(Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // Orders table
        Schema::table('orders', function(Blueprint $table) {
            $table->foreign('order_group_id')
                ->references('id')
                ->on('order_groups')
                ->onUpdate('cascade')
                ->onDelete('cascade');
//            $table->foreign('item_id')
//                ->references('id')
//                ->on('items')
//                ->onUpdate('cascade')
//                ->onDelete('cascade');
        });

        // Order groups table
        Schema::table('order_groups', function(Blueprint $table) {
            $table->foreign('customer_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // Cart items pivot table
        Schema::table('cart_items', function(Blueprint $table) {
//            $table->foreign('item_id')
//                ->references('id')
//                ->on('items')
//                ->onUpdate('cascade')
//                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // No need to create any foreign keys when dropping tables
    }
}
