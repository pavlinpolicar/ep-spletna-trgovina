<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->string('display');
            $table->text('description')->nullable();
            $table->double('price')->unsigned();
            $table->boolean('active')->default(true);

                // Foreign keys
            $table->integer('vendor_id')->unsigned();

            $table->timestamps();
        });
        DB::statement('ALTER TABLE items ADD FULLTEXT search(display, description)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Schema::table('items',
            function ($table) {
                $table->dropIndex('search');
            });

        Schema::drop('items');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
