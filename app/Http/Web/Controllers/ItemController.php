<?php

namespace App\Http\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\CreateRatingRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Http\Requests\UpdateRatingRequest;
use App\Models\Item;
use App\Models\Rating;
use App\Repositories\ItemRepository;
use App\Repositories\RatingRepository;
use Exception;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Support\Facades\Auth;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function index(ServerRequestInterface $request)
    {
        $query = new Item();

        $query = $this->parseQueryString($request->getQueryParams(), $query);

        return view('items.index')
            ->with('items', $query->paginate(5));
    }

    /**
     * Get all the items for a given user.
     *
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function allForUser(ServerRequestInterface $request)
    {
        $user = Auth::user();
        if (!$user->isVendor()) {
            return back();
        }
        return view('items.selling')
            ->with('items', $user->items()->orderBy('display', 'asc')->paginate(5));
    }

    /**
     * Display the specified resource.
     *
     * @param Item $item
     * @return Response
     */
    public function retrieve(Item $item)
    {
        return view('items.show')->with('item', $item);
    }

    /**
     * Display a form to add a new item.
     *
     * @return $this
     */
    public function add()
    {
        $this->authorize('create', Item::class);
        return view('items.new')->with('item', new Item());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateItemRequest $request
     * @return Response
     * @throws HttpException
     */
    public function create(CreateItemRequest $request)
    {
        $this->authorize('create', Item::class);
        ItemRepository::createItem($request->all(), Auth::user());
        return redirect()->route('web.items.index');
    }

    /**
     * Create a new rating for the item.
     *
     * @param CreateRatingRequest $request
     * @param Item $item
     * @return Response
     * @throws HttpException
     */
    public function createRating(CreateRatingRequest $request, Item $item)
    {
        $this->authorize('createRating', $item);
        RatingRepository::createRatingForItemByUser($request->all(), $item, Auth::user());
        return back();
    }

    /**
     * Display the edit page for the item.
     *
     * @param Item $item
     * @return Response
     * @throws HttpException
     */
    public function edit(Item $item)
    {
        $this->authorize('update', $item);
        return view('items.edit')->with('item', $item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateItemRequest $request
     * @param Item $item
     * @return Response
     * @throws HttpException
     * @throws MassAssignmentException
     */
    public function update(UpdateItemRequest $request, Item $item)
    {
        $this->authorize('update', $item);
        $item->fill($request->all());
        if (!$request->has('active')) {
            $item->active = false;
        }
        $item->save();
        return redirect()->route('web.selling.index');
    }

    /**
     * Update an existing rating for the item.
     *
     * @param UpdateRatingRequest $request
     * @param Rating $rating
     * @return Response
     * @throws HttpException
     * @throws MassAssignmentException
     */
    public function updateRating(UpdateRatingRequest $request, Rating $rating)
    {
        $this->authorize('update', $rating);
        $rating->fill($request->all())->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Item $item
     * @return Response
     * @throws Exception
     */
    public function delete(Item $item)
    {
        $this->authorize('delete', $item);
        $item->delete();
        return back();
    }

    /**
     * Delete an existing rating for the item.
     *
     * @param Rating $rating
     * @return Response
     * @throws Exception
     */
    public function deleteRating(Rating $rating)
    {
        $this->authorize('delete', $rating);
        $rating->delete();
        return back();
    }

    protected function parseQueryString($options, $query)
    {
        if (array_key_exists('query', $options)) {
            $query = $query->whereRaw(
                'MATCH (display, description) AGAINST (? IN BOOLEAN MODE)',
                [$options['query']]
            );
        }

        if (array_key_exists('sort', $options)) {
            $sorts = explode('|', $options['sort']);
            foreach ($sorts as $tmp) {
                $method = explode('.', $tmp);
                $query = $query->orderBy($method[0], $method[1]);
            }
        } else {
            $query = $query->orderBy('display', 'asc');
        }

        return $query;
    }
}
