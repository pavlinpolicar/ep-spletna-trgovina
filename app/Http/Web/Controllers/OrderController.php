<?php

namespace App\Http\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\PlaceOrderRequest;
use App\Http\Requests\UpdateOrderStatusRequest;
use App\Models\Order;
use App\Models\OrderGroup;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class OrderController extends Controller
{
    /**
     * Get a listing of all orders for the appropriate user.
     *
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function index(ServerRequestInterface $request)
    {
        $user = Auth::user();

        $orders = Collection::make();

        if ($user->isVendor()) {
            // if vendor return list of items that have been ordered for vendor
            $items = $user->items()->get();

            foreach ($items as $item) {
                $orders = $orders->merge(
                    $this->parseQueryString($request->getQueryParams(), $item->orders())->get()
                );
            }

        } elseif ($user->isCustomer()) {
            // if customer, return list of all orders placed by customer
            $orderGroups = $user->orderGroups()->get();

            foreach ($orderGroups as $group) {
                $orders = $orders->merge(
                    $this->parseQueryString($request->getQueryParams(), $group->orders())->get()
                );
            }

        }

        return view('orders.index')
            ->with('pendingOrders',
                $orders->filter(function ($order) {
                    return $order->status === Order::STATUS_PENDING;
                }))
            ->with('cancelledOrders',
                $orders->filter(function ($order) {
                    return $order->status === Order::STATUS_CANCELLED;
                }))
            ->with('pastOrders',
                $orders->filter(function ($order) {
                    return $order->status === Order::STATUS_DELIVERED;
                }));
    }

    /**
     * Show the order detils
     *
     * @param Order $order
     * @return $this
     */
    public function show(Order $order)
    {
        return view('orders.show')->with('order', $order);
    }

    /**
     * Create an order for authenticated user.
     *
     * @param PlaceOrderRequest $request
     * @return Request
     * @throws MassAssignmentException
     * @throws HttpException
     */
    public function createOrder(PlaceOrderRequest $request)
    {
        $this->authorize('create', OrderGroup::class);
        return OrderRepository::createOrderGroupFromArrayForUser($request->all(), Auth::user());
    }

    /**
     * Update the order status.
     *
     * @param UpdateOrderStatusRequest $request
     * @param Order $order
     * @return Request
     * @throws HttpException
     */
    public function updateOrderStatus(UpdateOrderStatusRequest $request, Order $order)
    {
        $this->authorize('updateStatus', $order);
        OrderRepository::changeOrderStatusTo($order, $request->status);
        return back();
    }

    /**
     * Query string options for the resource.
     *
     * @param array $queries
     * @param $dbQuery
     * @return mixed
     */
    protected function parseQueryString(array $queries, $dbQuery)
    {
        if (array_key_exists('filters', $queries)) {
            $dbQuery = $this->parseFilters($dbQuery, $queries['filters']);
        }

        if (array_key_exists('startDate', $queries)) {
            $dbQuery = $dbQuery->where('created_at',
                '>',
                $this->parseDateFromString($queries['startDate']));
        }

        if (array_key_exists('endDate', $queries)) {
            $dbQuery = $dbQuery->where('created_at',
                '>',
                $this->parseDateFromString($queries['endDate']));
        }

        if (array_key_exists('item_id', $queries)) {
            $dbQuery = $dbQuery->where('item_id', $queries['item_id']);
        }

        return $dbQuery;
    }

    /**
     * Parse the date from a localized date format.
     *
     * @param $strDate
     * @return static
     * @throws InvalidArgumentException
     */
    protected function parseDateFromString($strDate)
    {
        $date = Carbon::createFromFormat('d.m.Y', $strDate);
        $date->second = 0;
        $date->minute = 0;
        $date->hour = 0;
        return $date;
    }

    /**
     * Parse available filters and append them to the existing query.
     *
     * @param $dbQuery
     * @param $filters
     * @return mixed
     */
    protected function parseFilters($dbQuery, $filters)
    {
        return $dbQuery->where(function ($dbQuery) use ($filters) {
            $filters = explode(',', $filters);
            foreach ($filters as $filter) {
                switch ($filter) {
                    case 'pending':
                        $dbQuery = $dbQuery->orWhere('status',
                            Order::$orderStatus[Order::STATUS_PENDING]);
                        break;
                    case 'delivered':
                        $dbQuery = $dbQuery->orWhere('status',
                            Order::$orderStatus[Order::STATUS_DELIVERED]);
                        break;
                    case 'cancelled':
                        $dbQuery = $dbQuery->orWhere('status',
                            Order::$orderStatus[Order::STATUS_CANCELLED]);
                        break;
                    default:
                        // no op
                }
            }
            return $dbQuery;
        });
    }
}
