<?php

namespace App\Http\Web\Controllers;

use App\Models\Image;
use App\Models\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Item $item
     * @return \Illuminate\Http\Response
     */
    public function index(Item $item)
    {
        return view('images.index')
            ->with('item', $item)
            ->with('images', $item->images);
    }

    /**
     * Display a form to create a new instance of a resource.
     *
     * @param Item $item
     * @return $this
     */
    public function create(Item $item)
    {
        return view('images.new')
            ->with('item', $item);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Item $item
     * @return \Illuminate\Http\Response
     * @throws HttpException
     */
    public function store(Request $request, Item $item)
    {
        $this->authorize('uploadImage', $item);
        if ($request->hasFile('images')) {
            $images = $request->file('images');

            foreach ($images as $image) {

                $objImage = new Image;
                // Save image as hash so as to avoid collisions when uploading files named the same
                $objImage->name = md5($image->getClientOriginalName() . Carbon::now());
                $item->images()->save($objImage);

                $image->move($item->getImageDirectory(), $objImage->name);
            }
        }
        return back();
    }

    /**
     * Display the view to select the display image for a given item.
     *
     * @param Item $item
     * @return mixed
     */
    public function selectProfileGet(Item $item)
    {
        return view('images.profile')
            ->with('item', $item)
            ->with('images', $item->images);
    }

    /**
     * Change the display image on a given item.
     *
     * @param Item $item
     * @param Image $image
     * @return \Illuminate\Http\RedirectResponse
     */
    public function selectProfilePost(Item $item, Image $image)
    {
        $item->display_image_id = $image->id;
        $item->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Image $image
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Item $item, Image $image)
    {
        $image->delete();
        return back();
    }
}
