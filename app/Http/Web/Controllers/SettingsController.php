<?php

namespace App\Http\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Support\Facades\Auth;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SettingsController extends Controller
{
    public function index()
    {
        return view('settings.index')->with('user', Auth::user());
    }

    public function saveInfo(UpdateUserRequest $request)
    {
        $user = Auth::user();
        $this->authorize('update', $user);
        $user->fill($request->all())->save();
        return back();
    }

    public function savePassword(UpdateUserRequest $request)
    {
        $user = Auth::user();
        $user->password = bcrypt($request->password);
        $user->save();
        return back();
    }
}
