<?php

namespace App\Http\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\CartRepository;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    /**
     * Show a preview of all the items the current user is about to purchase.
     *
     * @return $this
     */
    public function index()
    {
        return view('checkout.index')
            ->with('items', Auth::user()->cart);
    }

    /**
     * Perform the purchase and clear all the purchased items from the users cart.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkout()
    {
        $user = Auth::user();
        $orders = [
            'items' => $user->cart->map(function ($item) {
                return ['id' => $item->id, 'quantity' => $item->pivot->quantity];
            })
        ];
        OrderRepository::createOrderGroupFromArrayForUser($orders, $user);
        // Clear the cart after purchase
        $user->cart->each(function ($item) use ($user) {
            CartRepository::removeItemFromCart($item, $user);
        });
        return back();
    }
}
