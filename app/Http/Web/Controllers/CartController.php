<?php

namespace App\Http\Web\Controllers;

use App\Exceptions\ItemAlreadyInCartException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\AddItemToCartRequest;
use App\Http\Requests\UpdateItemQuantityRequest;
use App\Models\Item;
use App\Repositories\CartRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CartController extends Controller
{
    /**
     * Get a list of all items that the authenticated user has in their cart.
     *
     * @return Request
     */
    public function index()
    {
        return view('cart.index')
            ->with('items', Auth::user()->cart()->paginate(10));
    }

    /**
     * Add an item to the authenticated users cart.
     *
     * @param AddItemToCartRequest $request
     * @param Item $item
     * @return Response
     * @throws ItemAlreadyInCartException
     */
    public function addItem(AddItemToCartRequest $request, Item $item)
    {
        CartRepository::addItemToUserCart($item, Auth::user(), $request->quantity);
        return back();
    }

    /**
     * Update an existing items quantity.
     *
     * @param UpdateItemQuantityRequest $request
     * @param Item $item
     * @return Response
     * @throws \App\Exceptions\ItemNotInCartException
     */
    public function updateItemQuantity(UpdateItemQuantityRequest $request, Item $item)
    {
        CartRepository::updateItemQuantityInCart($item, Auth::user(), $request->quantity);
        return back();
    }

    /**
     * Remove an item from the authenticated users cart.
     *
     * @param Item $item
     * @return Response
     */
    public function removeItem(Item $item)
    {
        CartRepository::removeItemFromCart($item, Auth::user());
        return back();
    }
}
