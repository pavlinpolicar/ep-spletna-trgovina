<?php

namespace App\Http\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\UserLogEntry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     * @throws HttpException
     */
    public function index()
    {
        $this->authorize('view', UserLogEntry::class);

        return view('logs.index')
            ->with('logs', UserLogEntry::orderBy('created_at', 'desc')->paginate(20));
    }
}
