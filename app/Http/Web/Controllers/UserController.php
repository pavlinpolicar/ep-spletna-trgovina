<?php

namespace App\Http\Web\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Exception;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Support\Facades\Auth;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ServerRequestInterface $request
     * @return Response
     * @throws HttpException
     */
    public function index(ServerRequestInterface $request)
    {
        $this->authorize('view', User::class);

        $user = Auth::user();

        if ($user->isAdmin()) {
            // Admin can only see and modify themselves and vendors
            return view('users.index')
                ->with('users', User::vendors()->paginate(20));

        } else if ($user->isVendor()) {
            // Vendors can only see and modify themselves and customers
            return view('users.index')
                ->with('users', User::customers()->paginate(20));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Response
     * @throws HttpException
     */
    public function retrieve(User $user)
    {
        $this->authorize('viewDetails', $user);
        return $user;
    }

    public function add()
    {
        return view('users.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserRequest $request
     * @return Response
     * @throws HttpException
     */
    public function create(CreateUserRequest $request)
    {
        $tempUser = new User();
        // must be assigned this way as this is a temporary field and not mass assignable
        $tempUser->permissionLevelType = $request->permission_level;
        $this->authorize('create', $tempUser);
        $user = UserRepository::createUserWithFields($request->all(), Auth::user());
        return back();
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user);
        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @return Response
     * @throws HttpException
     * @throws MassAssignmentException
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', $user);
        $user->fill($request->all());
        if (!$request->has('active')) {
            $user->active = false;
        }
        $user->save();
        return redirect()->route('web.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return Response
     * @throws Exception
     */
    public function delete(User $user)
    {
        $this->authorize('delete', $user);
        $user->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }
}
