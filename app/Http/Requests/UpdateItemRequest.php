<?php

namespace App\Http\Requests;

/**
 * Class UpdateItemRequest
 *
 * @property string|null display
 * @property string|null description
 * @property string|null price
 * @package App\Http\Requests
 */
class UpdateItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display' => 'max:255',
            'description' => '',
            'price' => 'numeric|min:0',
            'active' => 'boolean',
        ];
    }
}
