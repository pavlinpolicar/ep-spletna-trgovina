<?php

namespace App\Http\Requests;

/**
 * Class UpdateUserRequest
 *
 * @property string|null name
 * @property string|null surname
 * @property string|null email
 * @property string|null password
 * @property string|null confirm_password
 * @property string|null permission_level
 * @property string|null active
 * @property string|null address
 * @property string|null telephone
 * @package App\Http\Requests
 */
class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:255',
            'surname' => 'max:255',
            'email' => 'email|max:255',

            'password' => 'min:6|max:255|confirmed',

            'permission_level' => 'exists:permission_levels,type',

            'active' => 'boolean',

            'address' => 'max:255',
            'telephone' => 'max:255',
        ];
    }
}
