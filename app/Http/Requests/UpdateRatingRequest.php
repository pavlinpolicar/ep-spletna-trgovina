<?php

namespace App\Http\Requests;

/**
 * Class UpdateRatingRequest
 *
 * @property string|null rating
 * @property string|null description
 * @package App\Http\Requests
 */
class UpdateRatingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rating' => 'numeric|between:0,10',
            'description' => '',
        ];
    }
}
