<?php

namespace App\Http\Requests;

/**
 * Class CreateRatingRequest
 *
 * @property string rating
 * @property string|null description
 * @package App\Http\Requests
 */
class CreateRatingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rating' => 'required|numeric|between:0,10',
            'description' => '',
        ];
    }
}
