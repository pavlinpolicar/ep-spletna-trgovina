<?php

namespace App\Http\Requests;

/**
 * Class CreateItemRequest
 *
 * @property string display
 * @property string|null description
 * @property string price
 * @package App\Http\Requests
 */
class CreateItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display' => 'required|max:255',
            'description' => '',
            'price' => 'required|numeric|min:0',
        ];
    }
}
