<?php

namespace App\Http\Requests;

/**
 * Class AddItemToCartRequest
 *
 * @property string item_id
 * @property string quantity
 * @package App\Http\Requests
 */
class AddItemToCartRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required|numeric|min:0',
        ];
    }
}
