<?php

namespace App\Http\Requests;

use InvalidArgumentException;

class PlaceOrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public function rules()
    {
        $rules = [
            'items' => 'required|array',
        ];

        $items = $this->request->get('items');
        foreach ($items as $key => $item) {
            $rules['items.' . $key . '.id'] = 'required|exists:items,id';
            $rules['items.' . $key . '.quantity'] = 'required|numeric|min:1';
        }

        return $rules;
    }
}
