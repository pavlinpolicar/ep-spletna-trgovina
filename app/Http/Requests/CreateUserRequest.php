<?php

namespace App\Http\Requests;

use App\Models\PermissionLevel;

/**
 * Class CreateUserRequest
 *
 * @property string name
 * @property string surname
 * @property string email
 * @property string password
 * @property string confirm_password
 * @property string permission_level
 * @property string|null active
 * @property string|null address
 * @property string|null telephone
 * @package App\Http\Requests
 */
class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email',

            'password' => 'required|max:255',
            'confirm_password' => 'required|same:password',

            'permission_level' => 'required|exists:permission_levels,type',

            'active' => 'boolean',

            // Address and telephone are required if the user is a customer
            'address' => 'max:255|required_if:permission_level,' . PermissionLevel::CUSTOMER,
            'telephone' => 'max:255|required_if:permission_level,' . PermissionLevel::CUSTOMER,
        ];
    }
}
