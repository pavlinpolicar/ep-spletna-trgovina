<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// API routes
Route::group([
    'domain' => 'api.ep-store.local',
    'prefix' => 'v1',
    'namespace' => 'Api\V1\Controllers',
], function () {

    // Public routes
    Route::group([], function () {

        Route::get('items', [
            'as' => 'api.items.index',
            'uses' => 'ItemController@index',
        ]);
        Route::get('items/{item}', [
            'as' => 'api.items.retrieve',
            'uses' => 'ItemController@retrieve',
        ]);
        Route::get('items/{item}/ratings', [
            'as' => 'api.items.ratings',
            'uses' => 'ItemController@ratings'
        ]);

        Route::get('users/{user}/items', [
            'as' => 'api.users.items',
            'uses' => 'ItemController@allForUser',
        ]);

    });

    // Routes that need to be accessed via SSL, but not authenticated
    Route::group(['middleware' => ['require-ssl']], function () {

        Route::post('register', [
            'as' => 'api.register',
            'uses' => 'RegistrationController@register',
        ]);

    });

    // Routes that require the user to be logged in
    Route::group(
        ['middleware' => ['auth.basic.once', 'require-ssl', 'check-certificate', 'logger']],
        function () {

            Route::get('users/me', [
                'as' => 'api.users.me',
                'uses' => 'UserController@me',
            ]);

            Route::get('users', [
                'as' => 'api.users.index',
                'uses' => 'UserController@index',
            ]);
            Route::get('users/{user}', [
                'as' => 'api.users.retrieve',
                'uses' => 'UserController@retrieve',
            ]);
            Route::post('users', [
                'as' => 'api.users.create',
                'uses' => 'UserController@create',
            ]);
            Route::put('users/{user}', [
                'as' => 'api.users.update',
                'uses' => 'UserController@update',
            ]);
            Route::delete('users/{user}', [
                'as' => 'api.users.delete',
                'uses' => 'UserController@delete',
            ]);

            Route::post('items', [
                'as' => 'api.items.create',
                'uses' => 'ItemController@create',
            ]);
            Route::put('items/{item}', [
                'as' => 'api.items.update',
                'uses' => 'ItemController@update',
            ]);
            Route::delete('items/{item}', [
                'as' => 'api.items.delete',
                'uses' => 'ItemController@delete',
            ]);
            Route::post('items/{item}/upload', [
                'as' => 'api.items.image.upload',
                'uses' => 'ImageController@uploadItemImages'
            ]);
            Route::put('items/{item}/images/{image}', [
                'as' => 'api.items.image.update',
                'uses' => 'ImageController@updateItemImage'
            ]);
            Route::delete('items/{item}/images/{image}', [
                'as' => 'api.items.image.delete',
                'uses' => 'ImageController@removeItemImage'
            ]);

            Route::post('items/{item}/ratings', [
                'as' => 'api.items.ratings.create',
                'uses' => 'ItemController@createRating',
            ]);
            Route::put('items/{item}/ratings/{rating}', [
                'as' => 'api.items.ratings.update',
                'uses' => 'ItemController@updateRating',
            ]);
            Route::delete('items/{item}/ratings/{rating}', [
                'as' => 'api.items.ratings.delete',
                'uses' => 'ItemController@deleteRating',
            ]);

            Route::get('cart', [
                'as' => 'api.cart.index',
                'uses' => 'CartController@index',
            ]);
            Route::post('cart/{item}', [
                'as' => 'api.cart.addItem',
                'uses' => 'CartController@addItem',
            ]);
            Route::put('cart/{item}', [
                'as' => 'api.cart.updateItem',
                'uses' => 'CartController@updateItem',
            ]);
            Route::delete('cart/{item}', [
                'as' => 'api.cart.removeItem',
                'uses' => 'CartController@removeItem',
            ]);

            Route::get('orders', [
                'as' => 'api.orders.index',
                'uses' => 'OrderController@index',
            ]);
            Route::post('orders', [
                'as' => 'api.orders.create',
                'uses' => 'OrderController@createOrder',
            ]);
            Route::put('orders/{order}/status', [
                'as' => 'api.orders.updateStatus',
                'uses' => 'OrderController@updateOrderStatus',
            ]);

            Route::get('logs/{user}', [
                'as' => 'api.logs.user',
                'uses' => 'UserLogController@user'
            ]);

    });

});

// WEB routes
Route::group([
    'domain' => 'ep-store.local',
    'namespace' => 'Web\Controllers',
    'middleware' => ['csrf']
], function () {

    Route::get('/', function () {
        return redirect()->route('web.items.index');
    });

    // Routes that need to be accessed via SSL
    Route::group(['middleware' => ['require-ssl']], function () {

        Route::get('login', [
            'as' => 'web.login',
            'uses' => 'Auth\AuthController@getLogin',
        ]);
        Route::post('login', [
            'as' => 'web.login.post',
            'uses' => 'Auth\AuthController@postLogin',
        ]);
        Route::get('logout', [
            'as' => 'web.logout',
            'uses' => 'Auth\AuthController@getLogout',
        ]);

        Route::get('register', [
            'as' => 'web.register',
            'uses' => 'Auth\AuthController@getRegister',
        ]);
        Route::post('register', [
            'as' => 'web.register.post',
            'uses' => 'Auth\AuthController@postRegister',
        ]);

    });

    // Routes that require the user to be logged in
    Route::group(
        ['middleware' => ['auth', 'require-ssl', 'check-certificate', 'logger']],
        function () {

            Route::get('selling', [
                'as' => 'web.selling.index',
                'uses' => 'ItemController@allForUser',
            ]);
            Route::get('items/new', [
                'as' => 'web.items.new',
                'uses' => 'ItemController@add',
            ]);
            Route::post('items', [
                'as' => 'web.items.create',
                'uses' => 'ItemController@create',
            ]);
            Route::get('items/{item}/edit', [
                'as' => 'web.items.edit',
                'uses' => 'ItemController@edit',
            ]);
            Route::put('items/{item}', [
                'as' => 'web.items.update',
                'uses' => 'ItemController@update',
            ]);

            Route::get('items/{item}/images', [
                'as' => 'web.items.images.index',
                'uses' => 'ImageController@index',
            ]);
            Route::get('items/{item}/images/new', [
                'as' => 'web.items.images.create',
                'uses' => 'ImageController@create',
            ]);
            Route::post('items/{item}/images', [
                'as' => 'web.items.images.store',
                'uses' => 'ImageController@store',
            ]);
            Route::delete('items/{item}/images/{image}', [
                'as' => 'web.items.images.delete',
                'uses' => 'ImageController@destroy',
            ]);
            Route::get('items/{item}/images/profile', [
                'as' => 'web.items.images.profile',
                'uses' => 'ImageController@selectProfileGet',
            ]);
            Route::put('items/{item}/images/{image}/profile', [
                'as' => 'web.items.images.profile.select',
                'uses' => 'ImageController@selectProfilePost',
            ]);

            Route::post('items/{item}/ratings', [
                'as' => 'web.items.ratings.create',
                'uses' => 'ItemController@createRating',
            ]);
            Route::put('items/{item}/ratings/{rating}', [
                'as' => 'web.items.ratings.update',
                'uses' => 'ItemController@updateRating',
            ]);
            Route::delete('items/{item}/ratings/{rating}', [
                'as' => 'web.items.ratings.delete',
                'uses' => 'ItemController@deleteRating',
            ]);

            Route::get('cart', [
                'as' => 'web.cart.index',
                'uses' => 'CartController@index',
            ]);
            Route::post('cart/{item}', [
                'as' => 'web.cart.addItem',
                'uses' => 'CartController@addItem',
            ]);
            Route::put('cart/{item}', [
                'as' => 'web.cart.updateItem',
                'uses' => 'CartController@updateItemQuantity',
            ]);
            Route::delete('cart/{item}', [
                'as' => 'web.cart.removeItem',
                'uses' => 'CartController@removeItem',
            ]);

            Route::get('orders', [
                'as' => 'web.orders.index',
                'uses' => 'OrderController@index',
            ]);
            Route::get('orders/{order}', [
                'as' => 'web.orders.show',
                'uses' => 'OrderController@show',
            ]);
            Route::put('orders/{order}/status', [
                'as' => 'web.orders.updateStatus',
                'uses' => 'OrderController@updateOrderStatus',
            ]);

            Route::get('checkout', [
                'as' => 'web.checkout.index',
                'uses' => 'CheckoutController@index',
            ]);
            Route::post('checkout', [
                'as' => 'web.checkout.post',
                'uses' => 'CheckoutController@checkout',
            ]);

            Route::get('settings', [
                'as' => 'web.settings.index',
                'uses' => 'SettingsController@index',
            ]);
            Route::post('settings/info', [
                'as' => 'web.settings.info.save',
                'uses' => 'SettingsController@saveInfo',
            ]);
            Route::post('settings/password', [
                'as' => 'web.settings.password.save',
                'uses' => 'SettingsController@savePassword',
            ]);

            Route::get('users', [
                'as' => 'web.users.index',
                'uses' => 'UserController@index',
            ]);
            Route::get('users/new', [
                'as' => 'web.users.new',
                'uses' => 'UserController@add',
            ]);
            Route::post('users', [
                'as' => 'web.users.create',
                'uses' => 'UserController@create',
            ]);
            Route::get('users/{user}/edit', [
                'as' => 'web.users.edit',
                'uses' => 'UserController@edit',
            ]);
            Route::put('users/{user}', [
                'as' => 'web.users.update',
                'uses' => 'UserController@update',
            ]);

            Route::get('logs', [
                'as' => 'web.logs.index',
                'uses' => 'LogController@index',
            ]);

    });

    // Public routes
    Route::group([], function () {

        Route::get('items', [
            'as' => 'web.items.index',
            'uses' => 'ItemController@index',
        ]);
        Route::get('items/{item}', [
            'as' => 'web.items.retrieve',
            'uses' => 'ItemController@retrieve',
        ]);

    });

});