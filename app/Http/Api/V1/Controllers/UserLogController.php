<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use Illuminate\Http\Response;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ServerRequestInterface $request
     * @param User $user
     * @return Response
     * @throws HttpException
     */
    public function user(ServerRequestInterface $request, User $user)
    {
        $this->authorize('viewLog', $user);

        $query = $user->logEntries()
            ->orderBy('created_at', 'desc');

        if (array_key_exists('pageSize', $request->getQueryParams())) {
            return $query->paginate($request->getQueryParams()['pageSize']);
        } else {
            return $query->get();
        }
    }
}
