<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateItemImageRequest;
use App\Models\Image;
use App\Models\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageController extends Controller
{
    /**
     * Upload an image for the item.
     *
     * @param Request $request
     * @param Item $item
     */
    public function uploadItemImages(Request $request, Item $item)
    {
        $this->authorize('uploadImage', $item);
        if ($request->hasFile('images')) {
            $images = $request->file('images');

            foreach ($images as $image) {

                $objImage = new Image;
                // Save image as hash so as to avoid collisions when uploading files named the same
                $objImage->name = md5($image->getClientOriginalName() . Carbon::now());
                $item->images()->save($objImage);

                $image->move($item->getImageDirectory(), $objImage->name);
            }
        }
    }

    /**
     * Update an item display image id via the UpdateItemRequest.
     *
     * @param UpdateItemImageRequest $request
     * @param Item $item
     * @param Image $image
     * @return Item
     */
    public function updateItemImage(UpdateItemImageRequest $request, Item $item, Image $image)
    {
        if ($request->display) {
            $item->display_image_id = $image->id;
        } else {
            $item->display_image_id = null;
        }
        $item->save();
        return $item;
    }

    /**
     * Remove the image for the item.
     *
     * @param Item $item
     * @param Image $image
     * @return Response
     */
    public function removeItemImage(Item $item, Image $image)
    {
        $this->authorize('deleteImage', $item);

        if ($image->isDisplayImageOf($item)) {
            $item->displayImage = null;
        }
        $image->delete();

        // Delete file from disk
        unlink(public_path() . '/' . $item->getImageDirectory() . $image->name);

        return response('', Response::HTTP_NO_CONTENT);
    }
}
