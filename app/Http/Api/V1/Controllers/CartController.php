<?php

namespace App\Http\Api\V1\Controllers;

use App\Exceptions\ItemAlreadyInCartException;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\AddItemToCartRequest;
use App\Http\Requests\UpdateItemQuantityRequest;
use App\Models\Item;
use app\Repositories\CartRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Response;

class CartController extends Controller
{
    /**
     * Get a list of all items that the authenticated user has in their cart.
     *
     * @param ServerRequestInterface $request
     * @return Request
     */
    public function index(ServerRequestInterface $request)
    {
        if (array_key_exists('pageSize', $request->getQueryParams())) {
            return Auth::user()->cart()->paginate($request->getQueryParams()['pageSize']);
        } else {
            return Auth::user()->cart()->get();
        }
    }

    /**
     * Add an item to the authenticated users cart.
     *
     * @param AddItemToCartRequest $request
     * @param Item $item
     * @return Response
     * @throws ItemAlreadyInCartException
     */
    public function addItem(AddItemToCartRequest $request, Item $item)
    {
        CartRepository::addItemToUserCart($item, Auth::user(), $request->quantity);
        return response('', Response::HTTP_CREATED);
    }

    /**
     * Update an existing items quantity.
     *
     * @param UpdateItemQuantityRequest $request
     * @param Item $item
     * @return Response
     * @throws \App\Exceptions\ItemNotInCartException
     */
    public function updateItemQuantity(UpdateItemQuantityRequest $request, Item $item)
    {
        CartRepository::updateItemQuantityInCart($item, Auth::user(), $request->quantity);
        return response('', Response::HTTP_OK);
    }

    /**
     * Remove an item from the authenticated users cart.
     *
     * @param Item $item
     * @return Response
     */
    public function removeItem(Item $item)
    {
        CartRepository::removeItemFromCart($item, Auth::user());
        return response('', Response::HTTP_NO_CONTENT);
    }
}
