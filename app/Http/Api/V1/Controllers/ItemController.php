<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Http\Requests\UpdateRatingRequest;
use App\Models\Item;
use App\Models\Rating;
use App\Models\User;
use App\Repositories\ItemRepository;
use App\Repositories\RatingRepository;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Support\Facades\Auth;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function index(ServerRequestInterface $request)
    {
        $query = new Item();

        $query = $this->parseQueryString($request->getQueryParams(), $query);

        if (array_key_exists('pageSize', $request->getQueryParams())) {
            $result = $query->paginate($request->getQueryParams()['pageSize']);
        } else {
            $result = $query->get();
        }

        return response()->json($result, Response::HTTP_OK, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Get all the items for a given user.
     *
     * @param ServerRequestInterface $request
     * @param User $user
     * @return Response
     */
    public function allForUser(ServerRequestInterface $request, User $user)
    {
        if (array_key_exists('pageSize', $request->getQueryParams())) {
            $result = $user->items()->paginate($request->getQueryParams()['pageSize']);
        } else {
            $result = $user->items()->all();
        }

        return response()->json($result, Response::HTTP_OK, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Get the ratings for the item.
     *
     * @param ServerRequestInterface $request
     * @param Item $item
     * @return Response
     */
    public function ratings(ServerRequestInterface $request, Item $item)
    {
        if (array_key_exists('pageSize', $request->getQueryParams())) {
            $result = $item->ratings()->paginate($request->getQueryParams()['pageSize']);
        } else {
            $result = $item->ratings()->get();
        }

        return response()->json($result, Response::HTTP_OK, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Display the specified resource.
     *
     * @param Item $item
     * @return Response
     */
    public function retrieve(Item $item)
    {
        return response()->json($item->withImages(), Response::HTTP_OK, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateItemRequest $request
     * @return Response
     * @throws HttpException
     */
    public function create(CreateItemRequest $request)
    {
        $this->authorize('create', Item::class);
        $item = ItemRepository::createItem($request->all(), Auth::user());
        return response()->json($item, Response::HTTP_CREATED, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Create a new rating for the item.
     *
     * @param CreateRatingRequest $request
     * @param Item $item
     * @return Response
     * @throws HttpException
     */
    public function createRating(CreateRatingRequest $request, Item $item)
    {
        $this->authorize('createRating', $item);
        $item = RatingRepository::createRatingForItemByUser($request->all(), $item, Auth::user());
        return response()->json($item, Response::HTTP_CREATED, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateItemRequest $request
     * @param Item $item
     * @return Response
     * @throws HttpException
     * @throws MassAssignmentException
     */
    public function update(UpdateItemRequest $request, Item $item)
    {
        $this->authorize('update', $item);
        $item->fill($request->all())->save();
        return response()->json($item, Response::HTTP_OK, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Update an existing rating for the item.
     *
     * @param UpdateRatingRequest $request
     * @param Rating $rating
     * @return Response
     * @throws HttpException
     * @throws MassAssignmentException
     */
    public function updateRating(UpdateRatingRequest $request, Rating $rating)
    {
        $this->authorize('update', $rating);
        $rating->fill($request->all())->save();
        return response()->json($rating, Response::HTTP_OK, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Item $item
     * @return Response
     * @throws HttpException
     */
    public function delete(Item $item)
    {
        $this->authorize('delete', $item);
        $item->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Delete an existing rating for the item.
     *
     * @param Rating $rating
     * @return Response
     * @throws HttpException
     */
    public function deleteRating(Rating $rating)
    {
        $this->authorize('delete', $rating);
        $rating->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    protected function parseQueryString($options, $query)
    {
        if (array_key_exists('query', $options)) {
            $query = $query->whereRaw(
                'MATCH (display, description) AGAINST (? IN BOOLEAN MODE)',
                [$options['query']]
            );
        }

        if (array_key_exists('sort', $options)) {
            // TODO
        } else {
            $query = $query->orderBy('display', 'asc');
        }

        return $query;
    }
}
