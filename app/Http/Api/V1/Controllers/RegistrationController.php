<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Models\PermissionLevel;
use App\Repositories\UserRepository;
use Illuminate\Http\Response;

class RegistrationController extends Controller
{
    /**
     * Register a new user.
     *
     * @param RegisterRequest $request
     * @return Response
     */
    public function register(RegisterRequest $request)
    {
        $user = UserRepository::createUserWithPermission($request->all(),
            PermissionLevel::customer());
        return $user;
    }
}
