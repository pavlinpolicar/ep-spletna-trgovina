<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Support\Facades\Auth;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ServerRequestInterface $request
     * @return Response
     * @throws HttpException
     */
    public function index(ServerRequestInterface $request)
    {
        $this->authorize('view', User::class);

        $user = Auth::user();

        if ($user->isAdmin()) {
            // Admin can only see and modify themselves and vendors
            $query = User::orAdmins()->orVendors();

            if (array_key_exists('pageSize', $request->getQueryParams())) {
                return $query->paginate($request->getQueryParams()['pageSize']);
            } else {
                return $query->get();
            }

        } else if ($user->isVendor()) {
            // Vendors can only see and modify themselves and customers
            $query = User::orVendors()->orCustomers();

            if (array_key_exists('pageSize', $request->getQueryParams())) {
                return $query->paginate($request->getQueryParams()['pageSize']);
            } else {
                return $query->get();
            }

        }
    }

    /**
     * Get information  about the currently authenticated user.
     *
     * @return Response
     */
    public function me()
    {
        return Auth::user();
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Response
     * @throws HttpException
     */
    public function retrieve(User $user)
    {
        $this->authorize('viewDetails', $user);
        return $user;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserRequest $request
     * @return Response
     * @throws HttpException
     */
    public function create(CreateUserRequest $request)
    {
        $tempUser = new User();
        // must be assigned this way as this is a temporary field and not mass assignable
        $tempUser->permissionLevelType = $request->permission_level;
        $this->authorize('create', $tempUser);
        $user = UserRepository::createUserWithFields($request->all(), Auth::user());
        return response($user, Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @return Response
     * @throws HttpException
     * @throws MassAssignmentException
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', $user);
        $user->fill($request->all())->save();
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return Response
     * @throws HttpException
     */
    public function delete(User $user)
    {
        $this->authorize('delete', $user);
        $user->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }
}
