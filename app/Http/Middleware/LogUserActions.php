<?php

namespace App\Http\Middleware;

use App\Models\UserLogEntry;
use Closure;
use Illuminate\Support\Facades\Auth;

class LogUserActions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user->isAdmin() or $user->isVendor()) {

            $route = $request->route();

            $message = "Accessed {$route->getAction()['as']}";

            $user->logEntries()->create([
                'description' => $message
            ]);
        }

        return $next($request);
    }
}
