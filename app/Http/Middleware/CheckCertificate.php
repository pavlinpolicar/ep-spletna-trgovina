<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckCertificate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $certificate = filter_input(INPUT_SERVER, 'SSL_CLIENT_CERT');

        // Admin and vendors require X509 certificate for any action.
        if (($user->isAdmin() or $user->isVendor()) and
            ($certificate === null or $certificate === '')) {
            Auth::logout();
            return redirect()->guest('login');
        }

        return $next($request);
    }
}
