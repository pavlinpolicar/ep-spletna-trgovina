<?php

namespace App\Http\Middleware;

use Closure;

class RequireSsl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->isSecure()) {
            return redirect()->secure($request->getRequestUri());
        } else {
            return $next($request);
        }
    }
}
