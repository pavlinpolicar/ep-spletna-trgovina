<?php

namespace App\Policies;

use App\Models\Rating;
use App\Models\User;

class RatingPolicy extends BasePolicy
{
    /**
     * Check if the user has permission to update the rating.
     *
     * @param User $user
     * @param Rating $rating
     * @return bool
     */
    public function update(User $user, Rating $rating)
    {
        return $user->isSameUserAs($rating->user);
    }

    /**
     * Check if the user has permission to delete the rating.
     *
     * @param User $user
     * @param Rating $rating
     * @return bool
     */
    public function delete(User $user, Rating $rating)
    {
        return $user->isSameUserAs($rating->user);
    }
}
