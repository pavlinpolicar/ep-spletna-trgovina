<?php

namespace App\Policies;

use App\Models\Item;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ItemPolicy extends BasePolicy
{

    /**
     * Check if the user can see the item.
     *
     * @param User $user
     * @param Item $item
     * @return bool
     */
    public function see(User $user, Item $item)
    {
        return $item->isActive() or $item->isInactive() and $user->owns($item);
    }

    /**
     * Check if the user can sell any items.
     *
     * @param User $user
     * @return bool
     */
    public function sell(User $user)
    {
        return $user->isVendor();
    }

    /**
     * Check if the user has permission to create a resource.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isVendor();
    }

    /**
     * Check if the user has permission to create a rating for a resource.
     *
     * @param User $user
     * @param Item $item
     * @return mixed
     */
    public function createRating(User $user, Item $item)
    {
        return true;
    }

    /**
     * Check if the user has persmission to update the resource.
     *
     * @param User $user
     * @param Item $item
     * @return bool
     */
    public function update(User $user, Item $item)
    {
        return $user->isVendor() and $user->owns($item);
    }

    /**
     * Check if the user has persmission to delete a resource.
     *
     * @param User $user
     * @param Item $item
     * @return bool
     */
    public function delete(User $user, Item $item)
    {
        return $user->isVendor() and $user->owns($item);
    }

    /**
     * Check whether the user can upload an image for the item.
     *
     * @param User $user
     * @param Item $item
     * @return bool
     */
    public function uploadImage(User $user, Item $item)
    {
        return $user->owns($item);
    }

    /**
     * Check whether the user can delete the image.
     *
     * @param User $user
     * @param Image $image
     * @return bool
     */
    public function deleteImage(User $user, Image $image)
    {
        return $user->owns($image);
    }
}
