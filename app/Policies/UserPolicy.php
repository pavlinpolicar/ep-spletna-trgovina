<?php

namespace App\Policies;

use App\Models\PermissionLevel;
use App\Models\User;

class UserPolicy extends BasePolicy
{
    /**
     * Check if the user has permission to view a list of users.
     *
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->isAdmin() or $user->isVendor();
    }

    /**
     * Check if the user has permission to view the details of a target user.
     *
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function viewDetails(User $user, User $targetUser)
    {
        return
            // Allow admin to see all
            $user->isAdmin() or
            // Vendor can see other vendor details and customer details
            $user->isVendor() and ($targetUser->isVendor() or $targetUser->isCustomer()) or
            // Customers can only see their own details
            $user->isCustomer() and $user->isSameUserAs($targetUser);
    }

    /**
     * Check if the user has permission to view user logs.
     *
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function viewLog(User $user, User $targetUser)
    {
        return $user->isAdmin();
    }

    /**
     * Check if the user has permission to create the given user.
     *
     * @param User $user
     * @param User $newUser
     * @return bool
     */
    public function create(User $user, User $newUser)
    {
        $level = $newUser->permissionLevelType;
        // nobody can create another admin
        if (PermissionLevel::ADMIN === $level) {
            return false;
        }
        return
            // Admin can create anything except another admin
            $user->isAdmin() or
            // Vendor can create a customer
            $user->isVendor() and $level === PermissionLevel::CUSTOMER;
    }

    /**
     * Check if the user has persmission to update the given user.
     *
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function update(User $user, User $targetUser)
    {
        return
            // Admin can update themselves, vendors and customers
            $user->isAdmin() and $user->isSameUserAs($targetUser) or
            $user->isAdmin() and $targetUser->isVendor() or
            $user->isAdmin() and $targetUser->isCustomer() or
            // Vendors can update themselves and customers
            $user->isVendor() and $user->isSameUserAs($targetUser) or
            $user->isVendor() and $targetUser->isCustomer() or
            // Customers can only update themseleves
            $user->isCustomer() and $user->isSameUserAs($targetUser);
    }

    /**
     * Check if the user has persmission to delete a resource.
     *
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function delete(User $user, User $targetUser)
    {
        // Nobody has specific rights to delete users
        return false;
    }
}
