<?php

namespace app\Policies;


use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

abstract class BasePolicy
{
    use HandlesAuthorization;

    /**
     * Check if user is active before moving on to further checks.
     *
     * @param User $user
     * @return bool|null
     */
    public function before(User $user)
    {
        if ($user->isInactive()) {
            return false;
        }
    }
}