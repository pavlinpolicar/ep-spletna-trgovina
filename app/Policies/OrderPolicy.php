<?php

namespace App\Policies;


use App\Models\Order;
use App\Models\OrderGroup;
use App\Models\User;
use App\Repositories\CartRepository;

class OrderPolicy extends BasePolicy
{
    /**
     * Check whether the user has permission to place an order
     *
     * @param User $user
     * @param OrderGroup $group
     * @return bool
     */
    public function create(User $user, OrderGroup $group)
    {
        return
            $user->isCustomer() and
            CartRepository::userHasItemsInCart($user, $group->items()->get());
    }

    /**
     * Check whether the user has permission to change the status of the order.
     *
     * @param User $user
     * @param Order $order
     * @return bool
     */
    public function updateStatus(User $user, Order $order)
    {
        return $user->isSameUserAs($order->item->vendor);
    }
}