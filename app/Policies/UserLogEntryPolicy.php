<?php

namespace App\Policies;

use App\Models\Item;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserLogEntryPolicy extends BasePolicy
{
    /**
     * Check if the user can see the item.
     *
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->isAdmin();
    }
}
