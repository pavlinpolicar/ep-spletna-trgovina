<?php

namespace App\Exceptions;

use App\Models\Item;
use App\Models\User;
use Exception;

/**
 * @property User user
 * @property Item item
 */
class ItemNotInCartException extends Exception
{
    public function __construct($message, User $user, Item $item)
    {
        parent::__construct($message);

        $this->user = $user;
        $this->item = $item;
    }
}