<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Item
 *
 * @property int id
 * @property string display
 * @property string description
 * @property float price
 * @property User vendor
 * @property int numRatings
 * @property int numOrders
 * @property mixed averageRating
 * @property Image displayImage
 * @property Collection images
 * @package App\Models
 */
class Item extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['display', 'description', 'price', 'active'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['num_ratings', 'average_rating', 'num_orders', 'display_image_url'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['display_image_id'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'price' => 'float',
        'active' => 'boolean',
    ];

    /**
     * Check wheter the item is active.
     *
     * @return mixed
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Check whether the item is inactive.
     *
     * @return mixed
     */
    public function isInactive()
    {
        return $this->isActive();
    }

    /**
     * Convenient method to access the item vendor directly.
     *
     * @return mixed
     */
    public function getVendorAttribute()
    {
        return $this->vendors()->first();
    }

    /**
     * Get the number of ratings the item has.
     *
     * @return int
     */
    public function getNumRatingsAttribute()
    {
        return $this->ratings()->count();
    }

    /**
     * Get the number of times the item has been bought.
     *
     * @return int
     */
    public function getNumOrdersAttribute()
    {
        return $this->orders()->count();
    }

    /**
     * Get the average rating for the item.
     *
     * @return float
     */
    public function getAverageRatingAttribute()
    {
        return $this->ratings()->get()->avg('rating');
    }

    /**
     * Ge the images for the item.
     *
     * @return mixed
     */
    public function getImagesAttribute()
    {
        return $this->images()->get();
    }

    /**
     * Get the display picture for the item.
     *
     * @return mixed
     */
    public function getDisplayImageAttribute()
    {
        return $this->displayImage()->first();
    }

    /**
     * Get the display image url for the attribute.
     *
     * @return string
     */
    public function getDisplayImageUrlAttribute()
    {
        return $this->displayImage !== null ? $this->displayImage->url : null;
    }

    /**
     * Get an array of all the urls for all the images for the item.
     *
     * @return static
     */
    public function getImagesUrlsAttribute()
    {
        return $this->images->map(function ($image) {
            return $image->url;
        });
    }

    /**
     * Get the directory in which the images for the item are stored.
     *
     * @return string
     */
    public function getImageDirectory()
    {
        return 'item_assets/' . $this->id . '/images/';
    }

    /**
     * Specify that the image urls should be returned with the response.
     *
     * @return $this
     */
    public function withImages()
    {
        $this->appends[] = 'images_urls';
        return $this;
    }

    /**
     * Get the display picture for the item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function displayImage()
    {
        return $this->hasOne(Image::class, 'id', 'display_image_id');
    }

    /**
     * All the users that have the item in their carts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function userCarts()
    {
        return $this->belongsToMany(User::class, 'cart_items', 'item_id', 'user_id')
            ->withPivot('quantity')
            ->withTimestamps();
    }

    /**
     * The vendor associated with the item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendors()
    {
        return $this->belongsTo(User::class, 'vendor_id');
    }

    /**
     * All the orders that have been made with the item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * All the ratings given to the item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /**
     * All the images associated with the item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
