<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserLogEntry
 *
 * @property int id
 * @property string description
 * @property User user
 * @package App\Models
 */
class UserLogEntry extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_log_entries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        // make sure to create a created_at timestamp upon log entry creation
        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }

    /**
     * The user that this entry belongs to.
     *
     * @return User
     */
    public function getUserAttribute()
    {
        return $this->user()->first();
    }

    /**
     * Get the user associated with the log entry.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
