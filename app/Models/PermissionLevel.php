<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PermissionLevel
 *
 * @property int id
 * @property int type
 * @property string display
 * @property string description
 * @package App\Models
 */
class PermissionLevel extends Model
{
    /**
     * Permission levels.
     */
    const ADMIN = 30;
    const VENDOR = 20;
    const CUSTOMER = 10;

    /**
     * Allowed permission levels.
     */
    public static $permissionLevelType = [
        self::ADMIN => 'ADMIN',
        self::VENDOR => 'VENDOR',
        self::CUSTOMER => 'CUSTOMER',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permission_levels';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'display', 'description'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'type' => 'integer',
    ];

    /**
     * Get the admin permission level instance.
     *
     * @return mixed
     */
    public static function admin()
    {
        return PermissionLevel::where('type', PermissionLevel::ADMIN)->first();
    }

    /**
     * Get the vendor permission level instance.
     *
     * @return mixed
     */
    public static function vendor()
    {
        return PermissionLevel::where('type', PermissionLevel::VENDOR)->first();
    }

    /**
     * Get the customer permission level instance.
     *
     * @return mixed
     */
    public static function customer()
    {
        return PermissionLevel::where('type', PermissionLevel::CUSTOMER)->first();
    }

    /**
     * All the users that have the selected permission levle.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
