<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class OrderGroup
 *
 * @property int id
 * @property Collection orders
 * @property User customer
 * @package App\Models
 */
class OrderGroup extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Orders accessor.
     *
     * @return Collection
     */
    public function getOrdersAttribute()
    {
        return $this->orders()->get();
    }

    /**
     * Customer accessor.
     *
     * @return User
     */
    public function getCustomerAttribute()
    {
        return $this->customer()->first();
    }

    /**
     * Get the orders that belong to this order group.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * Get the user that placed the order group.
     *
     * @return mixed
     */
    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }
}
