<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rating
 *
 * @property int id
 * @property int rating
 * @property string review
 * @property User user
 * @property Item item
 * @property int item_id
 * @property int user_id
 * @package App\Models
 */
class Rating extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ratings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['rating', 'review', 'item_id', 'user_id'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'rating' => 'integer',
    ];

    /**
     * The user that created the rating.
     *
     * @return User
     */
    public function getUserAttribute()
    {
        return $this->user()->first();
    }

    /**
     * The item the rating is for.
     *
     * @return Item
     */
    public function getItemAttribute()
    {
        return $this->item()->first();
    }

    /**
     * The user that sumbitted the rating.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The item that the rating was submitted for.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
