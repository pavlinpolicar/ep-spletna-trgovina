<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 *
 * @property int id
 * @property string name
 * @property Item item
 * @property string url
 * @package App\Models
 */
class Image extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Get the item that this image belongs to.
     *
     * @return mixed
     */
    public function getItemAttribute()
    {
        return $this->item()->first();
    }

    /**
     * Get the url of the image.
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return url($this->item->getImageDirectory() . $this->name);
    }

    /**
     * Check whether the image is the display image of the item.
     *
     * @param Item $item
     * @return bool
     */
    public function isDisplayImageOf(Item $item)
    {
        if ($item->displayImage === null) {
            return false;
        }
        return $this->id === $item->displayImage->id;
    }

    /**
     * Get the image associated with the given item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
