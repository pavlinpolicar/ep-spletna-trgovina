<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 *
 * @property int id
 * @property int quantity
 * @property int status
 * @property OrderGroup orderGroup
 * @property Item item
 * @package App\Models
 */
class Order extends Model
{
    /**
     * Order status codes.
     */
    const STATUS_PENDING = 1;
    const STATUS_DELIVERED = 2;
    const STATUS_CANCELLED = 3;

    /**
     * Allowed order status codes.
     */
    public static $orderStatus = [
        self::STATUS_PENDING => 'PENDING',
        self::STATUS_DELIVERED => 'DELIVERED',
        self::STATUS_CANCELLED => 'CANCELLED',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['quantity', 'status', 'item_id'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'quantity' => 'integer',
    ];

    /**
     * Order group accessor.
     *
     * @return OrderGroup
     */
    public function getOrderGroupAttribute()
    {
        return $this->orderGroup()->first();
    }

    /**
     * Item accessor.
     *
     * @return Item
     */
    public function getItemAttribute()
    {
        return $this->item()->first();
    }

    /**
     * Get the status number so it can be compared with the defined constants.
     *
     * @return int
     */
    public function getStatusAttribute()
    {
        return array_search($this->attributes['status'], static::$orderStatus);
    }

    /**
     * Set the status if the given value is a valid status.
     *
     * @param int $value
     */
    public function setStatusAttribute($value)
    {
        if (array_key_exists($value, static::$orderStatus)) {
            $this->attributes['status'] = static::$orderStatus[$value];
        }
    }

    /**
     * Get the order group that the order belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderGroup()
    {
        return $this->belongsTo(OrderGroup::class);
    }

    /**
     * Get the item that belong to this order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
