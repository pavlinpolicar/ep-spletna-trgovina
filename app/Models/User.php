<?php

namespace App\Models;

use App\Repositories\CartRepository;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Support\Collection;

/**
 * Class User
 *
 * @property int id
 * @property string name
 * @property string surname
 * @property string email
 * @property string password
 * @property int permissionLevelType
 * @property bool active
 * @package App\Models
 */
class User extends Model
    implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'permission_level_id',
        'active',
        'address',
        'telephone'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * Get the users active attribute.
     *
     * @param $value
     * @return bool
     */
    public function getActiveAttribute($value)
    {
        return (bool)$value;
    }

    /**
     * Get the users permission level type.
     *
     * @return int
     */
    public function getPermissionLevelTypeAttribute()
    {
        return (int)$this->permissionLevel()->first()->type;
    }

    /**
     * Set the users permission level type.
     *
     * @param int $level
     */
    public function setPermissionLevelTypeAttribute($level)
    {
        if (array_key_exists($level, PermissionLevel::$permissionLevelType)) {
            $this->permissionLevelType = $level;
        }
    }

    /**
     * Check if the user is an administrator.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->permissionLevelType === PermissionLevel::ADMIN;
    }

    /**
     * Check if the user is a vendor.
     *
     * @return bool
     */
    public function isVendor()
    {
        return $this->permissionLevelType === PermissionLevel::VENDOR;
    }

    /**
     * Check if the user is a customer.
     *
     * @return bool
     */
    public function isCustomer()
    {
        return $this->permissionLevelType === PermissionLevel::CUSTOMER;
    }

    /**
     * Check if the user is active.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Check if the user is inactive.
     *
     * @return bool
     */
    public function isInactive()
    {
        return !$this->isActive();
    }

    /**
     * The permission level associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function permissionLevel()
    {
        return $this->belongsTo(PermissionLevel::class);
    }

    /**
     * Check if the user is selling a given item.
     *
     * @param Item $item
     * @return bool
     */
    public function owns(Item $item)
    {
        return $this->id === $item->vendor->id;
    }

    public function getNewOrders()
    {
        $orders = Collection::make();
        $items = $this->items()->get();

        foreach ($items as $item) {
            $orders = $orders->merge($item->orders()->where('status', 'PENDING')->get());
        }
        return $orders;
    }

    /**
     * Check if a given user is the same user as a given user.
     *
     * @param User $user
     * @return bool
     */
    public function isSameUserAs(User $user)
    {
        return $this->id === $user->id;
    }

    /**
     * All the items that the user is selling.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(Item::class, 'vendor_id');
    }

    /**
     * All the items that the user has in their cart.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cart()
    {
        return $this->belongsToMany(Item::class, 'cart_items', 'user_id', 'item_id')
            ->withPivot('quantity')
            ->withTimestamps();
    }

    /**
     * Check whether the user has an item in their cart.
     *
     * @param Item $item
     * @return bool
     */
    public function hasItemInCart(Item $item)
    {
        return CartRepository::userHasItemInCart($this, $item);
    }

    /**
     * All the order groups that the user has previously purchased.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderGroups()
    {
        return $this->hasMany(OrderGroup::class, 'customer_id');
    }

    /**
     * All the ratings that the user has made.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /**
     * Get all the users that the selected user has created.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function createdUsers()
    {
        return $this->hasMany(User::class, 'created_by');
    }

    /**
     * The user that created the selected user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Get all the log entries that are associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logEntries()
    {
        return $this->hasMany(UserLogEntry::class);
    }

    /**
     * Get users that are administrators.
     *
     * @param $query
     * @return mixed
     */
    public function scopeAdmins($query)
    {
        return $query->where('permission_level_id', PermissionLevel::admin()->id);
    }

    /**
     * Get users that are administrators or some other criteria.
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrAdmins($query)
    {
        return $query->orWhere('permission_level_id', PermissionLevel::admin()->id);
    }

    /**
     * Get users that are vendors.
     *
     * @param $query
     * @return mixed
     */
    public function scopeVendors($query)
    {
        return $query->where('permission_level_id', PermissionLevel::vendor()->id);
    }

    /**
     * Get users that are vendors or some other criteria.
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrVendors($query)
    {
        return $query->orWhere('permission_level_id', PermissionLevel::vendor()->id);
    }

    /**
     * Get users that are customers.
     *
     * @param $query
     * @return mixed
     */
    public function scopeCustomers($query)
    {
        return $query->where('permission_level_id', PermissionLevel::customer()->id);
    }

    /**
     * Get users that are customers or some other criteria.
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrCustomers($query)
    {
        return $query->orWhere('permission_level_id', PermissionLevel::customer()->id);
    }

    /**
     * Get only active users.
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
     * Get only inactive users.
     *
     * @param $query
     * @return mixed
     */
    public function scopeInactive($query)
    {
        return $query->where('active', false);
    }
}
