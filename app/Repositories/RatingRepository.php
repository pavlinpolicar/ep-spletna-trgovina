<?php

namespace App\Repositories;


use App\Models\Item;
use App\Models\Rating;
use App\Models\User;

class RatingRepository
{
    /**
     * Create a rating for a given item by a given user.
     *
     * @param array $properties
     * @param Item $item
     * @param User $user
     * @return mixed
     */
    public static function createRatingForItemByUser(array $properties, Item $item, User $user)
    {
        $rating = new Rating($properties);
        $rating->user_id = $user->id;
        $rating->item_id = $item->id;
        $rating->save();
        return Rating::find($rating->id);
    }
}