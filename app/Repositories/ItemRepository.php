<?php

namespace App\Repositories;


use App\Models\Item;
use App\Models\User;

class ItemRepository
{
    /**
     * Create an item belonging to a given user.
     *
     * @param array $properties
     * @param User $creatingUser
     * @return Item
     */
    public static function createItem(array $properties, User $creatingUser)
    {
        return $creatingUser->items()->create($properties);
    }
}