<?php

namespace App\Repositories;


use App\Models\PermissionLevel;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    /**
     * Create a user with the fields one would typically get from a request.
     *
     * @param array $fields
     * @param User $creatingUser
     * @return User
     */
    public static function createUserWithFields(array $fields, User $creatingUser)
    {
        $permissionLevel = PermissionLevel::where('type', $fields['permission_level'])->first();
        return self::createUserWithPermission($fields, $permissionLevel, $creatingUser);
    }

    /**
     * Create an admin user with the given user properties.
     *
     * @param $properties
     * @param User $creatingUser
     * @return User
     */
    public static function createAdmin(array $properties, User $creatingUser)
    {
        return self::createUserWithPermission($properties, PermissionLevel::admin(), $creatingUser);
    }

    /**
     * Create a vendor user with the given user properties.
     *
     * @param $properties
     * @param User $creatingUser
     * @return User
     */
    public static function createVendor(array $properties, User $creatingUser)
    {
        return self::createUserWithPermission($properties, PermissionLevel::vendor(),
            $creatingUser);
    }

    /**
     * Create a customer user with the given user properties.
     *
     * @param $properties
     * @param User $creatingUser
     * @return User
     */
    public static function createCustomer(array $properties, User $creatingUser)
    {
        return self::createUserWithPermission($properties, PermissionLevel::customer(),
            $creatingUser);
    }

    /**
     * Create a user with given properties and with a given permission.
     *
     * @param array $properties
     * @param PermissionLevel $permission
     * @param User $creatingUser
     * @return User
     */
    public static function createUserWithPermission(
        array $properties,
        PermissionLevel $permission,
        User $creatingUser = null
    ) {
        $user = new User($properties);
        // make sure to hash the password upon create
        $user->password = Hash::make($user->password);
        $permission->users()->save($user);
        if ($creatingUser !== null) {
            $creatingUser->createdUsers()->save($user);
        }
        // need to refresh user, so as to get default values from database
        return User::find($user->id);
    }
}