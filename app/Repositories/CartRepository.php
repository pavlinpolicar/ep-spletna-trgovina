<?php

namespace App\Repositories;


use App\Exceptions\ItemAlreadyInCartException;
use App\Exceptions\ItemNotInCartException;
use App\Models\Item;
use App\Models\User;
use Illuminate\Support\Collection;

class CartRepository
{
    /**
     * Check whether or not the user already has an item in their cart.
     *
     * @param User $user
     * @param Item $item
     * @return bool
     */
    public static function userHasItemInCart(User $user, Item $item)
    {
        return $user->cart()->get()->contains('id', $item->id);
    }

    /**
     * Check whether or not the user a collection of items in their cart.
     *
     * @param User $user
     * @param Collection $items
     * @return bool
     */
    public static function userHasItemsInCart(User $user, Collection $items)
    {
        foreach ($items as $item) {
            if (!static::userHasItemInCart($user, $item)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Add an given quantity of a given item to a users cart.
     *
     * @param Item $item
     * @param User $user
     * @param $quantity
     * @return void
     * @throws ItemAlreadyInCartException
     */
    public static function addItemToUserCart(Item $item, User $user, $quantity)
    {
        if (self::userHasItemInCart($user, $item)) {
            throw new ItemAlreadyInCartException('User already has item in cart', $user, $item);
        }
        $user->cart()->attach($item, ['quantity' => $quantity]);
    }

    /**
     * Change the quantity of an item in a users cart.
     *
     * @param Item $item
     * @param User $user
     * @param $quantity
     * @return void
     * @throws ItemNotInCartException
     */
    public static function updateItemQuantityInCart(Item $item, User $user, $quantity)
    {
        if (!self::userHasItemInCart($user, $item)) {
            throw new ItemNotInCartException('User does not have item in cart', $user, $item);
        }
        $user->cart()->updateExistingPivot($item->id, ['quantity' => $quantity]);
    }

    /**
     * Remove an item from a users cart. If the item didn't exist, do nothing.
     *
     * @param Item $item
     * @param User $user
     */
    public static function removeItemFromCart(Item $item, User $user)
    {
        if (self::userHasItemInCart($user, $item)) {
            $user->cart()->detach($item->id);
        }
    }
}