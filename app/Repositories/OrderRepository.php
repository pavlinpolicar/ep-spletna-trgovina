<?php

namespace App\Repositories;


use App\Models\Order;
use App\Models\OrderGroup;
use App\Models\User;
use Illuminate\Database\Eloquent\MassAssignmentException;

class OrderRepository
{
    /**
     * Create an order group with all corresponding orders for the items passed in the data array.
     *
     * @param array $data
     * @param User $user
     * @return OrderGroup
     * @throws MassAssignmentException
     */
    public static function createOrderGroupFromArrayForUser(array $data, User $user)
    {
        $items = $data['items'];

        $group = $user->orderGroups()->create([]);

        foreach ($items as $item) {
            self::createOrder($item, $group);
        }

        return $group;
    }

    /**
     * Create an order for given group for item.
     *
     * @param array $item
     * @param OrderGroup $group
     * @return Order
     * @throws MassAssignmentException
     */
    public static function createOrder(array $item, OrderGroup $group)
    {
        $order = new Order();
        $order->fill([
            'quantity' => $item['quantity'],
            'item_id' => $item['id'],
            'order_group_id' => $group->id,
        ]);
        $group->orders()->save($order);
        return $order;
    }

    /**
     * Change the order status.
     *
     * @param Order $order
     * @param int $newStatus
     * @return Order
     */
    public static function changeOrderStatusTo(Order $order, $newStatus)
    {
        // no need to access database if order status is already the same as the new status
        if ($order->status !== $newStatus) {
            $order->status = $newStatus;
            $order->save();
        }
        return $order;
    }
}