<?php

namespace App\Providers;

use App\Models\Image;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderGroup;
use App\Models\Rating;
use App\Models\User;
use App\Models\UserLogEntry;
use App\Policies\CartPolicy;
use App\Policies\ImagePolicy;
use App\Policies\ItemPolicy;
use App\Policies\OrderPolicy;
use App\Policies\RatingPolicy;
use App\Policies\UserLogEntryPolicy;
use App\Policies\UserPolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Item::class => ItemPolicy::class,
        Rating::class => RatingPolicy::class,
        User::class => UserPolicy::class,
        OrderGroup::class => OrderPolicy::class,
        Order::class => OrderPolicy::class,
        UserLogEntry::class => UserLogEntryPolicy::class,

        // virtual resources
        'cart' => CartPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
    }
}
