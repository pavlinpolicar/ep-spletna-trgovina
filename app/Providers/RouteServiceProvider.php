<?php

namespace App\Providers;

use App\Models\Image;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderGroup;
use App\Models\Rating;
use App\Models\User;
use App\Models\UserLogEntry;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        // The default exception when a model is not found is a HttpNotFoundException. We override
        // this here
        $notFoundException = function () {
            throw new ModelNotFoundException();
        };

        // Bind models to route parmeters
        $router->model('item', Item::class, $notFoundException);
        $router->model('order', Order::class, $notFoundException);
        $router->model('orderGroup', OrderGroup::class, $notFoundException);
        $router->model('rating', Rating::class, $notFoundException);
        $router->model('user', User::class, $notFoundException);
        $router->model('userLogEntry', UserLogEntry::class, $notFoundException);

        $router->bind('image', function ($imageName) {
            return Image::where('name', $imageName)->first();
        });
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
